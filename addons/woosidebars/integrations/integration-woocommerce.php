<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WooSidebars_Integration_WooCommerce {
	private $taxonomies = array();

	/**
	 * Constructor.
	 * @since  1.1.0
	 */
	public function __construct () {
		add_filter( 'woo_conditions', array( &$this, 'register_conditions' ) );
		add_filter( 'woo_conditions_headings', array( &$this, 'register_conditions_headings' ) );
		add_filter( 'woo_conditions_reference', array( &$this, 'register_conditions_reference' ) );

		add_post_type_support( 'product', 'funding' );
	} // End __construct()

	/**
	 * Register the integration conditions with WooSidebars.
	 * @since  1.1.0
	 * @param  array $conditions The existing array of conditions.
	 * @return array             The modified array of conditions.
	 */
	public function register_conditions ( $conditions ) {
		global $post;

		if ( function_exists( 'is_woocommerce' ) && ! is_woocommerce() ) return $conditions;

		$integration = array();
		if ( function_exists( 'is_shop' ) && is_shop() ) $integration[] = 'wc-shop_page';
		if ( function_exists( 'is_product_category' ) && is_product_category() ) $integration[] = 'wc-product_category';
		if ( function_exists( 'is_product_tag' ) && is_product_tag() ) $integration[] = 'wc-product_tag';
		if ( function_exists( 'is_cart' ) && is_cart() ) $integration[] = 'wc-cart';
		if ( function_exists( 'is_checkout' ) && is_checkout() ) $integration[] = 'wc-checkout';
		if ( function_exists( 'is_account_page' ) && is_account_page() ) $integration[] = 'wc-account';

		if ( function_exists( 'is_product' ) && is_product() ) {
			$integration[] = 'wc-product';

			$categories = get_the_terms( $post->ID, 'product_cat' );

			if ( ! is_wp_error( $categories ) && is_array( $categories ) && ( count( $categories ) > 0 ) ) {
				foreach ( $categories as $k => $v ) {
					$integration[] = 'in-term-' . esc_attr( $v->term_id );
				}
			}

			$tags = get_the_terms( $post->ID, 'product_tag' );

			if ( ! is_wp_error( $tags ) && is_array( $tags ) && ( count( $tags ) > 0 ) ) {
				foreach ( $tags as $k => $v ) {
					$integration[] = 'in-term-' . esc_attr( $v->term_id );
				}
			}

		}

		$integration[] = $conditions[count($conditions)-1];

		array_splice( $conditions, count( $conditions ), 0, $integration );

		return $conditions;
	} // End register_conditions()

	/**
	 * Register the integration's headings for the meta box.
	 * @since  1.1.0
	 * @param  array $headings The existing array of headings.
	 * @return array           The modified array of headings.
	 */
	public function register_conditions_headings ( $headings ) {
		$headings['woocommerce'] = esc_html__( 'WooCommerce', 'funding' );

		return $headings;
	} // End register_conditions_headings()

	/**
	 * Register the integration's conditions reference for the meta box.
	 * @since  1.1.0
	 * @param  array $headings The existing array of conditions.
	 * @return array           The modified array of conditions.
	 */
	public function register_conditions_reference ( $conditions ) {
		$conditions['woocommerce'] = array();

		$conditions['woocommerce']['wc-shop_page'] = array(
									'label' => esc_html__( 'Shop Page', 'funding' ),
									'description' => esc_html__( 'The WooCommerce "Shop" landing page', 'funding' )
									);

		$conditions['woocommerce']['wc-product_category'] = array(
									'label' => esc_html__( 'Product Categories', 'funding' ),
									'description' => esc_html__( 'All product categories', 'funding' )
									);

		$conditions['woocommerce']['wc-product_tag'] = array(
									'label' => esc_html__( 'Product Tags', 'funding' ),
									'description' => esc_html__( 'All product tags', 'funding' )
									);

		$conditions['woocommerce']['wc-product'] = array(
									'label' => esc_html__( 'Products', 'funding' ),
									'description' => esc_html__( 'All products', 'funding' )
									);

		$conditions['woocommerce']['wc-cart'] = array(
									'label' => esc_html__( 'Cart Page', 'funding' ),
									'description' => esc_html__( 'The WooCommerce "Cart" page', 'funding' )
									);

		$conditions['woocommerce']['wc-checkout'] = array(
									'label' => esc_html__( 'Checkout Page', 'funding' ),
									'description' => esc_html__( 'The WooCommerce "Checkout" page', 'funding' )
									);

		$conditions['woocommerce']['wc-account'] = array(
									'label' => esc_html__( 'Account Pages', 'funding' ),
									'description' => esc_html__( 'The WooCommerce "Account" pages', 'funding' )
									);

		// Setup terminologies for the "in category" and "tagged with" conditions.
		$terminologies = array(
								'taxonomy-product_cat' => esc_html__( 'Products in the "%s" category', 'funding' ),
								'taxonomy-product_tag' => esc_html__( 'Products tagged "%s"', 'funding' )
							  );

			foreach ( $terminologies as $k => $v ) {
				if( ! isset( $conditions[$k] ) ) continue;
				foreach ( $conditions[$k] as $i => $j ) {
					$conditions[$k]['in-' . $i] = array( 'label' => sprintf( $terminologies[$k], $j['label'] ), 'description' => sprintf( $terminologies[$k], $j['label'] ) );
				}
			}

		return $conditions;
	} // End register_conditions_reference()
} // End Class

// Initialise the integration.
new WooSidebars_Integration_WooCommerce();
?>