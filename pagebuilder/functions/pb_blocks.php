<?php
/**
 * Page Builder Blocks
 * @desc Contains block elements to be inserted into custom page templates
 * @since 1.0.0
 * @todo add tooltip to explain option (hover on ? icon)
 */
// Text Block
function block_text($block) {
	extract( $block, EXTR_OVERWRITE );
	$block_id = 'block_' . $number;
	$block_saving_id = 'blocks[block_'.$number.']';
	?>
	<p class="description">
		<label for="<?php echo $block_id ?>_title">
			<?php esc_html_e("Title (optional)", 'funding'); ?>
			<input type="text" class="input-full" id="<?php echo $block_id ?>_title" value="<?php echo $title ?>" name="<?php echo $block_saving_id ?>[title]">
		</label>
	</p>
	<p class="description">
		<label for="<?php echo $block_id ?>_text">
			<?php esc_html_e("Content", 'funding'); ?>
			<textarea id="<?php echo $block_id ?>_text" class="textarea-full" name="<?php echo $block_saving_id ?>[text]" rows="5"><?php echo $text ?></textarea>
		</label>
	</p>
	<?php
}
//slogan
function block_slogan($block) {
	extract( $block, EXTR_OVERWRITE );
	$block_id = 'block_' . $number;
	$block_saving_id = 'blocks[block_'.$number.']';
	?>
	<p class="description">
		<label for="<?php echo $block_id ?>_title">
			<?php esc_html_e("Title (optional)", 'funding'); ?><br/>
			<input type="text" class="input-full" id="<?php echo $block_id ?>_title" value="<?php echo $title ?>" name="<?php echo $block_saving_id ?>[title]">
		</label>
	</p>
	<p class="description">
		<label for="<?php echo $block_id ?>_text">
			<?php esc_html_e("Enter your text slogan below", 'funding'); ?>
			<textarea id="<?php echo $block_id ?>_text" class="textarea-full" name="<?php echo $block_saving_id ?>[text]" rows="5"><?php echo $text ?></textarea>
		</label>
	</p>
	<?php
}
// Slider
function block_slider($block) {
	extract( $block, EXTR_OVERWRITE );
	$block_id = 'block_' . $number;
	$block_saving_id = 'blocks[block_'.$number.']';
	?>
	<p class="description">
		<label for="<?php echo $block_id ?>_title">
			<?php esc_html_e("Title (optional)", 'funding'); ?><br/>
			<input type="text" class="input-full" id="<?php echo $block_id ?>_title" value="<?php echo $title ?>" name="<?php echo $block_saving_id ?>[title]">
		</label>
	</p>
	<p class="description">
		<label for="<?php echo $block_id ?>_slide">
		<?php
		$args = array (
			'nopaging' => true,
			'post_type' => 'slider',
			'status' => 'publish',
		);
		$slides = get_posts($args);
		echo '<select id="'.$block_id.'" class="select" name="'.$block_saving_id.'[slide]">';
			echo '<option>Choose a slider</option>';
			foreach($slides as $slide) {
				echo '<option value="'.$slide->ID.'">'.htmlspecialchars($slide->post_title).'</option>';
			}
		echo '</select>';
		?>
		</label>
	</p>
	<p class="description description-float">
		<label for="<?php echo $block_id ?>_speed">
			<?php esc_html_e("Slider Speed", 'funding'); ?><br/>
			<input type="text" class="input-small" id="<?php echo $block_id ?>_speed" value="<?php echo $speed ?>" name="<?php echo $block_saving_id ?>[speed]">
		</label>
	</p>
	<p class="description description-float">
		<label for="<?php echo $block_id ?>_transition">
			<?php esc_html_e("Slider Speed", 'funding'); ?><br/>
			<input type="text" class="input-small" id="<?php echo $block_id ?>_speed" value="<?php echo $speed ?>" name="<?php echo $block_saving_id ?>[speed]">
		</label>
	</p>
	<?php
}
// Google Map
function block_googlemap($block) {
	extract( $block, EXTR_OVERWRITE );
	$block_id = 'block_' . $number;
	$block_saving_id = 'blocks[block_'.$number.']';
	?>
	<p class="description half">
		<label for="<?php echo $block_id ?>_title">
			<?php esc_html_e("Title (optional)", 'funding'); ?><br/>
			<input type="text" class="input-full" id="<?php echo $block_id ?>_title" value="<?php echo $title ?>" name="<?php echo $block_saving_id ?>[title]">
		</label>
	</p>
	<p class="description half last">
		<label for="<?php echo $block_id ?>_address">
			<?php esc_html_e("Address", 'funding'); ?><br/>
			<input type="text" class="input-full" id="<?php echo $block_id ?>_address" value="<?php echo $address ?>" name="<?php echo $block_saving_id ?>[address]">
		</label>
	</p>
	<p class="description two-third">
		<label for="<?php echo $block_id ?>_coordinates">
			<?php esc_html_e("Coordinates (optional) e.g. '3.82497,103.32390'", 'funding'); ?><br/>
			<input type="text" class="input-full" id="<?php echo $block_id ?>_coordinates" value="<?php echo $coordinates ?>" name="<?php echo $block_saving_id ?>[coordinates]">
		</label>
	</p>
	<p class="description third last">
		<label for="<?php echo $block_id ?>_height">
			<?php esc_html_e("Map height, in pixels.", 'funding'); ?><br/>
			<input type="number" class="input-min" id="<?php echo $block_id ?>_height" value="<?php echo $height ?>" name="<?php echo $block_saving_id ?>[height]"> &nbsp; px
		</label>
	</p>
	<?php
}
// Portfolio
function block_portfolio($block) {
	extract( $block, EXTR_OVERWRITE );
	$block_id = 'block_' . $number;
	$block_saving_id = 'blocks['.$block_id.']';
	$columns_options = array(
		'one' => 'Single',
		'two' => 'Two Columns',
		'three' => 'Three Columns',
		'four' => 'Four Columns',
	);
	//todo image as checkbox
	$types = ''; //get all portfolio 'type' taxonomy terms
	?>
	<p class="description">
		<label for="<?php echo $block_id ?>_title">
			<?php esc_html_e("Title (optional)", 'funding'); ?><br/>
			<input type="text" class="input-full" id="<?php echo $block_id ?>_title" value="<?php echo $title ?>" name="<?php echo $block_saving_id ?>[title]">
		</label>
	</p>
	<p class="description">
		<label for="">
			<?php esc_html_e("Number of Columns", 'funding'); ?><br/>
			<?php echo pb_select($columns_options, $columns, $block_id, 'columns'); ?>
		</label>
	</p>
	<?php
}
function block_featured_portfolio($block) {
	extract( $block, EXTR_OVERWRITE );
	$block_id = 'block_' . $number;
	$block_saving_id = 'blocks[block_'.$number.']';
	?>
	<p class="description">
		<label for="<?php echo $block_id ?>_title">
			<?php esc_html_e("Title (optional)", 'funding'); ?><br/>
			<input type="text" class="input-full" id="<?php echo $block_id ?>_title" value="<?php echo $title ?>" name="<?php echo $block_saving_id ?>[title]">
		</label>
	</p>
	<p class="description">
		<label for="<?php echo $block_id ?>_items">
			<?php esc_html_e("Maximum number of items", 'funding'); ?><br/>
			<input type="number" class="input-min" id="<?php echo $block_id ?>_items" value="<?php echo $items ?>" name="<?php echo $block_saving_id ?>[items]">
		</label>
	</p>
	<?php
}
function block_widgets($block) {
	extract( $block, EXTR_OVERWRITE );
	$block_id = 'block_' . $number;
	$block_saving_id = 'blocks[block_'.$number.']';
	//get all registered sidebars
	global $wp_registered_sidebars;
	$sidebar_options = array();
	foreach ($wp_registered_sidebars as $registered_sidebar) {
		$sidebar_options[$registered_sidebar['id']] = $registered_sidebar['name'];
	}
	?>
	<p class="description half">
		<label for="<?php echo $block_id ?>_title">
			<?php esc_html_e("Title (optional)", 'funding'); ?><br/>
			<input type="text" class="input-full" id="<?php echo $block_id ?>_title" value="<?php echo $title ?>" name="<?php echo $block_saving_id ?>[title]">
		</label>
	</p>
	<p class="description half last">
		<label for="">
			<?php esc_html_e("Choose sidebar/widget area", 'funding'); ?><br/>
			<?php echo pb_select($sidebar_options, $sidebar, $block_id, 'sidebar'); ?>
		</label>
	</p>
	<?php
}
function block_column($block) {
	echo '<p class="empty-column">',
	esc_html__('Drag block items into this box', 'funding'),
	'</p>';
	echo '<ul class="blocks column-blocks cf"></ul>';
}
function block_clear($block) {
	echo '<p class="description">',
	esc_html__('This block has no editable attributes. You can use it to clear the floats between two or more separate blocks vertically.', 'funding');
	echo '</p>';
}
/**
 * Ajax drag n drop slider handler
 *
 * This can be served as an example how you can provide custom
 * ajax handler and use in the blocks
 *
 * Also see the pb_config on adding extra js
 */
function ajax_slider_handler() {
}
function ajax_slider_display() {
}
?>