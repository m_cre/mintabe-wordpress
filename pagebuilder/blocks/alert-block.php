<?php
/** Notifications block **/

if(!class_exists('Alert_Block')) {
	class Alert_Block extends Block {

		//set and create block
		function __construct() {
			$block_options = array(
				'name' => esc_html__('Alerts', 'funding'),
				'size' => 'span6',
			);

			//create the block
			parent::__construct('alert_block', $block_options);
		}

		function form($instance) {

			$defaults = array(
				'content' => '',
				'type' => esc_html__('note', 'funding'),
				'style' => ''
			);
			$instance = wp_parse_args($instance, $defaults);
			extract($instance);

			$type_options = array(
				'default' => esc_html__('Standard', 'funding'),
				'info' => esc_html__('Info', 'funding'),
				'note' => esc_html__('Notification', 'funding'),
				'warn' => esc_html__('Warning', 'funding'),
				'tips' => esc_html__('Tips', 'funding')
			);

			?>

			<p class="description">
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>">
					<?php esc_html_e("Title (optional)", 'funding'); ?><br/>
					<?php echo field_input('title', $block_id, $title) ?>
				</label>
			</p>
			<p class="description">
				<label for="<?php echo esc_attr($this->get_field_id('content')); ?>">
					<?php esc_html_e("Alert Text (required)", 'funding'); ?><br/>
					<?php echo field_textarea('content', $block_id, $content) ?>
				</label>
			</p>
			<p class="description half">
				<label for="<?php echo esc_attr($this->get_field_id('type')); ?>">
					<?php esc_html_e("Alert Type", 'funding'); ?><br/>
					<?php echo field_select('type', $block_id, $type_options, $type) ?>
				</label>
			</p>
			<p class="description half last">
				<label for="<?php echo esc_attr($this->get_field_id('style')); ?>">
					<?php esc_html_e("Additional inline css styling (optional)", 'funding'); ?><br/>
					<?php echo field_input('style', $block_id, $style) ?>
				</label>
			</p>
			<?php

		}

		function pbblock($instance) {
			extract($instance);

			echo '<div class="alert '.esc_attr($type).' cf" style="'. $style .'">' . do_shortcode(htmlspecialchars_decode($content)) . '</div>';

		}

	}
}