<!DOCTYPE html>
<html <?php language_attributes(); ?>><head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <?php  global $page, $paged; ?>

    <?php include_once 'css/colours.php'; ?>
      <script type="text/javascript">
        var templateDir = "<?php echo esc_url(get_template_directory_uri()); ?>";
		var ajaxurl = "<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>";
        var premiumoption = "<?php echo esc_attr(of_get_option('premium_supporters')); ?>";
        var formatdatuma = "<?php echo esc_attr(funding_date_format_php_to_js(get_option('date_format'))); ?>";
    </script>

<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<div class="main_wrapper">
<?php $i = of_get_option('facebook_btn') ? 1 : 0; ?>
<?php $j = of_get_option('twitter_btn') ? 1 : 0; ?>
<?php $k = of_get_option('google_btn') ? 1 : 0; ?>

<?php if($i + $j + $k  == 1){$btnclass = 'one_btn';} ?>
<?php if($i + $j + $k  == 2){$btnclass = 'two_btn';} ?>
<?php if($i + $j + $k  == 3){$btnclass = 'three_btn';} ?>

<header>
    <div class="navbartop-wrapper" >
        <div class="container">
        <div class="search-wrapper">
            <?php include_once 'searchformprojects.php'; ?>
        </div>
        <div class="top-right">
           <?php if ( is_user_logged_in() ) { ?>
                <a href="<?php echo esc_url(wp_logout_url( home_url())) ?>" class="logout-top "><?php esc_html_e("Log out", 'funding'); ?></a>
                <?php  $user = wp_get_current_user();

                if(in_array('administrator', $user->roles)){ ?>
                <a href="<?php echo esc_url(admin_url()).'post-new.php?post_type=project' ?>" class="submit-top"><i class="icon-fire"></i><?php esc_html_e("Submit a project", 'funding'); ?></a>
                <?php }else{ ?>
                <a href="<?php echo get_permalink(get_ID_by_slug('launch-project')); ?>" class="submit-top"><i class="icon-fire"></i><?php esc_html_e("Submit a project", 'funding'); ?></a>
                <?php } ?>
                <a href="<?php echo get_permalink( get_page_by_path( 'my-account' ) ); ?>" class="account-top "><i class="icon-user" s></i><?php esc_html_e("My account", 'funding'); ?></a>
           <?php }else{ ?>
                <a href="#myModalL" role="button" class="login-top" data-toggle="modal"><?php esc_html_e("Login", 'funding'); ?></a>
                <a href="#myModalR" role="button" class="register-top" data-toggle="modal"><?php esc_html_e("Register", 'funding'); ?></a>
            <?php } ?>
            <ul class="social-media">
                <?php if ( of_get_option('facebook') ) { ?><li><a target="_blank" class="facebook"href="<?php echo esc_url(of_get_option('facebook_link')); ?>"><?php esc_html_e("facebook", 'funding'); ?></a></li><?php } ?>
                <?php if ( of_get_option('twitter') ) { ?><li><a target="_blank" class="twitter" href="<?php echo esc_url(of_get_option('twitter_link')); ?>"><?php esc_html_e("twitter", 'funding'); ?></a></li><?php } ?>
                <?php if ( of_get_option('rss') ) { ?><li><a target="_blank" class="rss" href="<?php echo esc_url(of_get_option('rss_link')); ?>"><?php esc_html_e("rss", 'funding'); ?></a></li><?php } ?>
                <?php if ( of_get_option('googleplus') ) { ?> <li><a target="_blank" class="google" href="<?php echo esc_url(of_get_option('google_link')); ?>"><?php esc_html_e("google", 'funding'); ?></a></li><?php } ?>
                <?php if ( of_get_option('skype') ) { ?><li><a target="_blank" class="skype" href="skype:<?php echo esc_url(of_get_option('skype_name')); ?>?add"><?php esc_html_e("skype", 'funding'); ?></a></li><?php } ?>
            </ul>

        </div>
 </div><!-- top right -->
</div><!-- Container -->
    <!-- NAVBAR
    ================================================== -->
 <div class="navbar-wrapper">
      <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
      <div class="container">
        <div class="logo-wrapper">
             <?php if (of_get_option('logo')!=""){ ?>
                 <a href="<?php  echo esc_url(home_url()); ?>"> <img src="<?php echo esc_url(of_get_option('logo')); ?>" alt="logo"  /> </a>
             <?php } ?>
        </div>
        <div class="navbar navbar-inverse">
          <div class="navbar-inner">
            <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            </a>
            <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->

            <div class="nav-collapse collapse">
                    <?php if(has_nav_menu('header-menu')) { ?>
              <?php wp_nav_menu( array( 'theme_location'  => 'header-menu', 'depth' => 0,'sort_column' => 'menu_order', 'items_wrap' => '<ul  class="nav">%3$s</ul>' ) ); ?>
                <?php }else { ?>
                   <ul  class="nav"><li>
                   <a href=""><?php esc_html_e('No menu assigned!', 'funding'); ?></a>
                   </li></ul>
                <?php } ?>
            </div><!--/.nav-collapse -->
          </div><!-- /.navbar-inner -->
        </div><!-- /.navbar -->
      </div> <!-- /.container -->
    </div><!-- /.navbar-wrapper -->
</header><!-- /.header -->

<div id="myModalL" class="modal hide fade <?php if(isset($btnclass)){echo esc_attr($btnclass);} ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?php esc_html_e("Login", 'funding'); ?></h3>
  </div>
  <div class="modal-body">
<?php
if ( is_user_logged_in() ) {
    global $current_user;
?>
<div id="LoginWithAjax">
    <?php
        global $current_user;
        global $user_level;
        global $wpmu_version;
        get_currentuserinfo();
    ?>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="avatar" id="LoginWithAjax_Avatar">
                <?php echo get_avatar( $current_user->ID, $size = '50' );  ?>
            </td>
            <td>
                  <a id="wp-logout" href="<?php echo esc_url(wp_logout_url( home_url())) ?>"><?php echo strtolower(esc_html__( 'Log Out', 'funding' )) ?></a><br />
            </td>
        </tr>
    </table>
</div>
<?php
    }else{
?>
    <div id="LoginWithAjax" class="default"><?php //ID must be here, and if this is a template, class name should be that of template directory ?>
        <span id="LoginWithAjax_Status"></span>
        <?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>
        <form name="LoginWithAjax_Form" id="LoginWithAjax_Form" action="<?php echo esc_url(home_url())?><?php echo (!is_plugin_active('better-wp-security/better-wp-security.php')) ? '/wp-login.php?' : '/?'; ?>callback=?&template=" method="post">
            <table width='100%' cellspacing="0" cellpadding="0">
                <tr id="LoginWithAjax_Username">
                    <td class="username_input">
                        <input type="text" name="log" placeholder="Username" id="lwa_user_login" class="input" value="" />
                    </td>
                </tr>
                <tr id="LoginWithAjax_Password">
                    <td class="password_input">
                        <input type="password" placeholder="Password" name="pwd" id="lwa_user_pass" class="input" value="" />
                    </td>
                </tr>
                <tr><td colspan="2"><?php do_action('login_form'); ?></td></tr>
                <tr id="LoginWithAjax_Submit">
                    <td id="LoginWithAjax_SubmitButton">
                         <input name="rememberme" type="checkbox" id="lwa_rememberme" value="forever" /> <label ><?php esc_html_e( 'Remember Me', 'funding' ) ?></label>
                        <a id="LoginWithAjax_Links_Remember"href="<?php echo esc_url(site_url('wp-login.php?action=lostpassword', 'login')); ?>" title="<?php esc_html_e('Password Lost and Found', 'funding') ?>"><?php esc_html_e('Lost your password?', 'funding') ?></a>
                        <br /><br />

                        <input type="submit"  class="button-green button-small"  name="wp-submit" id="lwa_wp-submit" value="<?php esc_html_e('Log In', 'funding'); ?>" tabindex="100" />
						<br />
						<div id="social_login" >
						<p><?php esc_html_e('Or login with:', 'funding'); ?></p>
                        	 <?php if(of_get_option('facebook_btn')){ ?>
                        	<a id='facebooklogin' class='button-medium facebookloginb'><i class='fa fa-facebook-square'></i><?php esc_html_e(' connect', 'funding');?></a>
                         <?php } ?>
                         <?php if(of_get_option('twitter_btn')){ ?>
                        <a id='twitterlogin' class='button-medium twitterloginb'><i class='fa fa-twitter-square'></i><?php esc_html_e(' connect', 'funding');?></a>
                         <?php } ?>
                         <?php if(of_get_option('google_btn')){ ?>
                        	<a id='googlelogin' class='button-medium googleloginb'><i class='fa fa fa-google-plus-square'></i><?php esc_html_e(' connect', 'funding');?></a>
                        <?php } ?>
                        </div>
                        <input type="hidden" name="redirect_to" value="http://<?php echo esc_attr($_SERVER['SERVER_NAME']) . esc_attr($_SERVER['REQUEST_URI']) ?>" />
                        <input type="hidden" name="testcookie" value="1" />
                        <input type="hidden" name="lwa_profile_link" value="<?php echo esc_url($lwa_data['profile_link']); ?>" />
                    </td>
                </tr>
            </table>
        </form>
        <form name="LoginWithAjax_Remember" id="LoginWithAjax_Remember" action="<?php echo esc_url(home_url())?><?php echo (!is_plugin_active('better-wp-security/better-wp-security.php')) ? '/wp-login.php?' : '/?'; ?>callback=?&template=" method="post">
            <table width='100%' cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <strong><?php echo esc_html__("Forgotten Password", 'funding'); ?></strong>
                    </td>
                </tr>
                <tr>
                    <td class="forgot-pass-email">
                        <?php $msg = esc_html__("Enter username or email", 'funding'); ?>
                        <input type="text" name="user_login" id="lwa_user_remember" value="<?php echo esc_attr($msg); ?>" onfocus="if(this.value == '<?php echo esc_attr($msg); ?>'){this.value = '';}" onblur="if(this.value == ''){this.value = '<?php echo esc_attr($msg); ?>'}" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" class="button-green button-small"  value="<?php echo esc_html__("Get New Password", 'funding'); ?>" />
                          <a href="#" id="LoginWithAjax_Links_Remember_Cancel"><?php esc_html_e("Cancel", 'funding'); ?></a>
                        <input type="hidden" name="login-with-ajax" value="remember" />

                    </td>
                </tr>
            </table>
        </form>
    </div>
<?php } ?>
  </div>
</div>
<div id="myModalR" class="modal hide fade <?php if(isset($btnclass)){echo esc_attr($btnclass);} ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?php esc_html_e("Register" , 'funding'); ?></h3>
  </div>
  <div class="modal-body">
    <div id="LoginWithAjax_Footer">
        <div id="LoginWithAjax_Register"  class="default">
                <span id="LoginWithAjax_Register_Status"></span>
            <h4 class="message register"><?php esc_html_e('Register For This Site', 'funding') ?></h4>
            <form name="LoginWithAjax_Register" id="LoginWithAjax_Register_Form" action="<?php echo esc_url(home_url()); ?>/wp-login.php?action=register&callback=?&template=" method="post">
                <p>
                    <label><input type="text" placeholder="Username" name="user_login" id="user_login" class="input" size="20" tabindex="10" /></label>
                </p>
                <p>
                    <label><input type="text" placeholder="E-mail" name="user_email" id="user_email" class="input" size="25" tabindex="20" /></label>
                </p>
                <?php do_action('register_form'); ?>
                <p id="reg_passmail"><?php esc_html_e('A password will be e-mailed to you.', 'funding') ?></p>
                <p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button-green button-small" value="<?php esc_html_e('Register', 'funding'); ?>" tabindex="100" /></p>
                <input type="hidden" name="lwa" value="1" />
            </form>
			<form name="LoginWithAjax_Form" id="LoginWithAjax_Form" action="<?php echo esc_url(home_url())?><?php echo (!is_plugin_active('better-wp-security/better-wp-security.php')) ? '/wp-login.php?' : '/?'; ?>callback=?&template=" method="post">
			<div id="social_login" class="reg" >
			 <p><?php esc_html_e('Or login with:', 'funding'); ?></p>
			  <?php if(of_get_option('facebook_btn')){ ?>
				<a id='facebooklogin' class='button-medium facebookloginb'><i class='fa fa-facebook-square'></i><?php esc_html_e(' connect', 'funding');?></a>
			 <?php } ?>
			   <?php if(of_get_option('twitter_btn')){ ?>
				<a id='twitterlogin' class='button-medium twitterloginb'><i class='fa fa-twitter-square'></i><?php esc_html_e(' connect', 'funding');?></a>
			<?php } ?>
			  <?php if(of_get_option('google_btn')){ ?>
				<a id='googlelogin' class='button-medium googleloginb'><i class='fa fa fa-google-plus-square'></i><?php esc_html_e(' connect', 'funding');?></a>
			<?php } ?>
			</div>
			</form>
        </div>
    </div>
  </div>
</div>



	<div id="myModalD" class="modal hide fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			    <h3><?php esc_html_e("Delete", 'funding'); ?></h3>
			  </div>
			  <div class="modal-body">
				<?php esc_html_e('Are you sure you want to delete this project?', 'funding'); ?><br/><br/>
				<script>

					jQuery( document ).ready(function() {
							var id = 0;
							var prid = jQuery('.dproject');
							prid.on( "click", function() {
							   var myClass = jQuery(this).attr("class");
							   myClass = myClass.replace(/\D/g,'');
							   id = parseInt(myClass, 10);

							});

							var conf = jQuery('#conf');
							conf.on( "click", function() {
								delete_project(id);
							});

					});

				</script>

				 <a id="conf" class="delete-button button-small button-red"><?php esc_html_e("Yes", "funding"); ?></a>

				 <button type="button" class="button-small button-grey" data-dismiss="modal" aria-hidden="true"><?php esc_html_e('No','funding'); ?></button>
			  </div>
			</div>


<?php if(is_page_template('tmp-home.php') or is_page_template('tmp-home-left.php') or is_page_template('tmp-home-right.php') or is_page_template('tmp-home-news.php') or is_page_template('tmp-no-title.php')){}elseif(is_search()){ ?>

<div class="container">
        <div class="span12">

        </div>
</div>
</div>
<?php }else{ ?>
<div class="page-title">
    <div class="container">
            <div class="span12">
                <div class="title-page">
                    <h1><?php
                 if ( is_plugin_active( 'woocommerce/woocommerce.php' )){
                    if (is_shop()){ echo get_the_title(skywarrior_get_id_by_slug ('shop'));}
                    else{ if(is_tag()){esc_html_e("Tag: ",'funding');echo get_query_var('tag' ); }elseif(is_category()){esc_html_e("Category: ",'funding');echo get_the_category_by_ID(get_query_var('cat'));}elseif(is_author()){esc_html_e("Author: ",'funding');echo get_the_author_meta('user_login', get_query_var('author' ));}elseif(is_archive()){ ?>
				  	<?php if ( is_day() ) : ?>
				        <?php printf( esc_html__( 'Daily Archives: %s', 'funding' ), get_the_date() ); ?>
				    <?php elseif ( is_month() ) : ?>
				        <?php printf( esc_html__( 'Monthly Archives: %s', 'funding' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'funding' ) ) ); ?>
				    <?php elseif ( is_year() ) : ?>
				        <?php printf( esc_html__( 'Yearly Archives: %s', 'funding' ), get_the_date( _x( 'Y', 'yearly archives date format', 'funding' ) ) ); ?>
				    <?php else : ?>
				        <?php esc_html_e( 'Blog Archives', 'funding' ); ?>
				    <?php endif; }else{the_title();} }
                 }else{  if(is_tag()){esc_html_e("Tag: ",'funding');echo get_query_var('tag' );}elseif(is_category()){esc_html_e("Category: ",'funding');echo get_the_category_by_ID(get_query_var('cat'));}elseif(is_author()){esc_html_e("Author: ",'funding');echo get_the_author_meta('user_login', get_query_var('author' ));}elseif(is_archive()){ ?>
				  	<?php if ( is_day() ) : ?>
				        <?php printf( esc_html__( 'Daily Archives: %s', 'funding' ), get_the_date() ); ?>
				    <?php elseif ( is_month() ) : ?>
				        <?php printf( esc_html__( 'Monthly Archives: %s', 'funding' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'funding' ) ) ); ?>
				    <?php elseif ( is_year() ) : ?>
				        <?php printf( esc_html__( 'Yearly Archives: %s', 'funding' ), get_the_date( _x( 'Y', 'yearly archives date format', 'funding' ) ) ); ?>
				    <?php else : ?>
				        <?php esc_html_e( 'Blog Archives', 'funding' ); ?>
				    <?php endif; }else{the_title();} } ?>
            </h1>
                </div>
                <div class="breadcrumbs"><strong><?php funding_breadcrumbs(); ?></strong></div>
            </div>
      </div>
</div>
<?php } ?>