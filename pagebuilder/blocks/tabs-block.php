<?php
/* Tabs Block */
if(!class_exists('Tabs_Block')) {
    class Tabs_Block extends Block {
        function __construct() {
            $block_options = array(
                'name' => esc_html__('Tabs &amp; Toggles', 'funding'),
                'size' => 'span6',
            );
            //create the widget
            parent::__construct('Tabs_Block', $block_options);
            //add ajax functions
            add_action('wp_ajax_block_tab_add_new', array($this, 'add_tab'));
        }
        function form($instance) {
            $defaults = array(
                 'content' => '',
                'tabs' => array(
                    1 => array(
                        'title' => esc_html__('My New Tab', 'funding'),
                        'content' => esc_html__('My tab contents', 'funding'),
                    )
                ),
                'type'  => 'tab',
                'position'  => 'top',
            );
            $instance = wp_parse_args($instance, $defaults);
            extract($instance);
            $tab_types = array(
                'tab' => esc_html__('Tabs', 'funding'),
                'toggle' => esc_html__('Toggles', 'funding'),
                'accordion' => esc_html__('Accordion', 'funding')
            );
            $tab_position = array(
                'top' => esc_html__('Top', 'funding'),
                'bottom' => esc_html__('Bottom', 'funding'),
                'left' => esc_html__('Left', 'funding'),
                'right' => esc_html__('Right', 'funding'),
            );
            ?>
            <div class="description cf">
                  <p class="description">
            <label for="<?php echo esc_attr(esc_attr($this->get_field_id('title'))); ?>">
                <?php esc_html_e("Title (optional)", 'funding') ?>
                <?php echo field_input('title', $block_id, $title, $size = 'full') ?>
            </label>
        </p>
                <ul id="sortable-list-<?php echo esc_attr($block_id); ?>" class="sortable-list" rel="<?php echo esc_attr($block_id); ?>">
                    <?php
                    $tabs = is_array($tabs) ? $tabs : $defaults['tabs'];
                    $count = 1;
                    foreach($tabs as $tab) {
                        $this->tab($tab, $count);
                        $count++;
                    }
                    ?>
                </ul>
                <p></p>
                <a href="#" rel="tab" class="sortable-add-new button"><?php esc_html_e("Add New", 'funding') ?></a>
                <p></p>
            </div>
            <p class="description">
                <label for="<?php echo esc_attr($this->get_field_id('type')); ?>">
                    <?php esc_html_e("Tabs style", 'funding') ?><br/>
                    <?php echo field_select('type', $block_id, $tab_types, $type) ?>
                </label>
            </p>
            <p class="description">
                <label for="<?php echo esc_attr($this->get_field_id('position')); ?>">
                    <?php esc_html_e("Tabs position", 'funding') ?><br/>
                    <?php echo field_select('position', $block_id, $tab_position, $position) ?>
                </label>
            </p>
            <?php
        }
        function tab($tab = array(), $count = 0) {
            ?>
            <li id="<?php echo esc_attr($this->get_field_id('tabs')); ?>-sortable-item-<?php echo esc_attr($count); ?>" class="sortable-item" rel="<?php echo esc_attr($count); ?>">
                <div class="sortable-head cf">
                    <div class="sortable-title">
                        <strong><?php echo esc_attr($tab['title']); ?></strong>
                    </div>
                    <div class="sortable-handle">
                        <a href="#"><?php esc_html_e("Open / Close", 'funding'); ?></a>
                    </div>
                </div>
                <div class="sortable-body">
                    <p class="tab-desc description">
                        <label for="<?php echo esc_attr($this->get_field_id('tabs')); ?>-<?php echo esc_attr($count); ?>-title">
                            <?php esc_html_e("Tab Title", 'funding'); ?><br/>
                            <input type="text" id="<?php echo esc_attr($this->get_field_id('tabs')); ?>-<?php echo esc_attr($count); ?>-title" class="input-full" name="<?php echo esc_attr($this->get_field_name('tabs')); ?>[<?php echo esc_attr($count); ?>][title]" value="<?php echo esc_attr($tab['title']); ?>" />
                        </label>
                    </p>
                    <p class="tab-desc description">
                        <label for="<?php echo esc_attr($this->get_field_id('tabs')); ?>-<?php echo esc_attr($count); ?>-content">
                            <?php esc_html_e("Tab Content", 'funding'); ?><br/>
                            <textarea id="<?php echo esc_attr($this->get_field_id('tabs')); ?>-<?php echo esc_attr($count); ?>-content" class="textarea-full" name="<?php echo esc_attr($this->get_field_name('tabs')); ?>[<?php echo esc_attr($count); ?>][content]" rows="5"><?php echo esc_attr($tab['content']); ?></textarea>
                        </label>
                    </p>
                    <p class="tab-desc description"><a href="#" class="sortable-delete"><?php esc_html_e("Delete", 'funding') ?></a></p>
                </div>
            </li>
            <?php
        }
        function pbblock($instance) {
            extract($instance);
             if($title) echo '<h3>'.esc_attr(strip_tags($title)).'</h3>';
            wp_enqueue_script('jquery-ui-tabs');
            $output = '';
            if($type == 'tab') {
                if($position == 'bottom'){
                    $output .= '<div id="block_tabs_'. rand(1, 100) .'" class="block_tabs"><div class="tab-inner">';
                    $i = 1;
                    foreach($tabs as $tab) {
                        $output .= '<div id="tab-'. sanitize_title( $tab['title'] ) . $i .'" class="tab tab-content">'. wpautop(do_shortcode(htmlspecialchars_decode($tab['content']))) .'</div>';
                        $i++;
                    }
                        $output .= '<ul class="nav cf nav-tabs">';
                    $i = 1;
                    foreach( $tabs as $tab ){
                        $tab_selected = $i == 1 ? 'ui-tabs-active' : '';
                        $output .= '<li class="'.$tab_selected.'"><a href="#tab-'. sanitize_title( $tab['title'] ) . $i .'">' . $tab['title'] . '</a></li>';
                        $i++;
                    }
                    $output .= '</ul>';
                $output .= '</div></div>';
                }else{
                $output .= '<div id="block_tabs_'. rand(1, 100) .'" class="block_tabs">';
                if($position == 'top'){
                $output .= '<div class="tab-inner">';
                }elseif($position == 'left'){
                $output .= '<div class="tabs-left">';
                }elseif($position == 'right'){
                $output .= '<div class="tabs-right">';
                }
                    $output .= '<ul class="nav cf nav-tabs">';
                    $i = 1;
                    foreach( $tabs as $tab ){
                        $tab_selected = $i == 1 ? 'ui-tabs-active' : '';
                        $output .= '<li class="'.$tab_selected.'"><a href="#tab-'. sanitize_title( $tab['title'] ) . $i .'">' . $tab['title'] . '</a></li>';
                        $i++;
                    }
                    $output .= '</ul>';
                    $i = 1;
                    foreach($tabs as $tab) {
                        $output .= '<div id="tab-'. sanitize_title( $tab['title'] ) . $i .'" class="tab tab-content">'. wpautop(do_shortcode(htmlspecialchars_decode($tab['content']))) .'</div>';
                        $i++;
                    }
                $output .= '</div></div>';
                }
            } elseif ($type == 'toggle') {
                $output .= '<div id="block_toggles_wrapper_'.rand(1,100).'" class="block_toggles_wrapper">';
                $i = 0;
                foreach( $tabs as $tab ){
                    $output  .= '<div class="block_toggle accordion-group ">';
                        $output .= '<h2 class="tab-head accordion-heading closed">'. $tab['title'] .'</h2>';
                        $output .= '<div class="arrow"></div>';
                        $output .= '<div id="'.$i.'" class="tab-body cf wcontainer" style="display: none;">';
                            $output .= wpautop(do_shortcode(htmlspecialchars_decode($tab['content'])));
                        $output .= '</div>';
                    $output .= '</div>';
                    $i++;
                }
                $output .= '</div>';
            } elseif ($type == 'accordion') {
                $count = count($tabs);
                $i = 1;
                $output .= '<div id="block_accordion_wrapper_'.rand(1,100).'" class="block_accordion_wrapper ">';
                foreach( $tabs as $tab ){
                    $open = $i == 1 ? 'open' : 'close';
                    $child = '';
                    if($i == 1) $child = 'first-child';
                    if($i == $count) $child = 'last-child';
                    $i++;
                        $output .= '<h3 class="tab-head accordion-heading"  style="-moz-user-select: none;-webkit-user-select: none;" onselectstart="return false;">&nbsp;&nbsp;'. $tab['title'] .'<span class="arrow"></span></h3> ';
                        $output .= '<div class="tab-body cf wcontainer" >';
                            $output .= wpautop(do_shortcode(htmlspecialchars_decode($tab['content'])));
                        $output .= '</div>';
                }
                $output .= '</div>';
            }
            echo $output;
        }
        /* AJAX add tab */
        function add_tab() {
            $nonce = $_POST['security'];
            if (! wp_verify_nonce($nonce, 'pb-settings-page-nonce') ) die('-1');
            $count = isset($_POST['count']) ? absint($_POST['count']) : false;
            $this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'block-9999';
            //default key/value for the tab
            $tab = array(
                'title' => esc_html__('New Tab', 'funding'),
                'content' => ''
            );
            if($count) {
                $this->tab($tab, $count);
            } else {
                die(-1);
            }
            die();
        }
        function update($new_instance, $old_instance) {
            $new_instance = recursive_sanitize($new_instance);
            return $new_instance;
        }
    }
}
