    <?php
/*
* Template name: All projects page
*/
?>
<?php get_header();?>

<div class="container all-projects">
  <div class="row">

    <div class="span12">
     <?php

        $args = array(
              'type' => 'project',
              'taxonomy' => 'project-category',
              'orderby' => 'name',
              'order' => 'ASC',
              );
            $categories = get_categories($args); ?>

            <ul id="category-menu">
                <?php foreach ( $categories as $cat ) { ?>
                <li id="cat-<?php echo esc_attr($cat->term_id); ?>"><a id="click" class="<?php echo esc_attr($cat->slug); ?> ajax" onclick="cat_ajax_get_all('<?php echo esc_attr($cat->term_id); ?>');" ><?php echo esc_attr($cat->name); ?></a></li>

                <?php } ?>
            </ul>
             <div id="loading-animation">
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/loading.gif"/>
            </div>
            <div id="category-post-content"></div>
    </div>
    <!-- /.span12 -->
  </div>
  <!-- /.row -->
</div>
<!-- /.container -->
<?php get_footer(); ?>