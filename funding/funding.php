<?php
// Include the origin controller
require_once (dirname(__FILE__).'/lib/Controller.php');
require_once (dirname(__FILE__).'/paypal.php');
require_once (dirname(__FILE__).'/globals.php');
require_once (dirname(__FILE__).'/admin.php');
function siteorigin_funding_activate(){
    F_Controller::action_init();
    flush_rewrite_rules();
}
register_activation_hook(__FILE__, 'siteorigin_funding_activate');
/**
 * Front end controller
 */
class F_Controller extends Origin_Controller{
      public function __construct(){
        return parent::__construct(false, 'f');
    }
    static function single($class){
    	 if(empty($class)) $class = __CLASS__;
        return parent::single(__CLASS__);
    }
    ///////////////////////////////////////////////////////////////////
    // Action Functions
    ///////////////////////////////////////////////////////////////////
    function action_init(){
        global $f_paypal;
        if(empty($_REQUEST['mode']) or empty($_REQUEST['app_id']) or empty($_REQUEST['api_username']) or empty($_REQUEST['api_password']) or empty($_REQUEST['api_signature']) or empty($_REQUEST['email'])){
            $f_paypal = get_option('funding_paypal');
            $mode = $f_paypal["mode"];
            $appid = $f_paypal["app_id"];
            $appusername = $f_paypal["api_username"];
            $apppassword = $f_paypal["api_password"];
            $appsingature = $f_paypal["api_signature"];
            $email = $f_paypal["email"];
            //sneak in wepay data
            $wepay_account_id = $f_paypal['wepay-account_id'];
        }else{

            $mode = $_REQUEST['mode'];
            $appid = $_REQUEST['app_id'];
            $appusername = $_REQUEST['api_username'];
            $apppassword = $_REQUEST['api_password'];
            $appsingature = $_REQUEST['api_signature'];
            $email = $_REQUEST['email'];
            //sneak in wepay data
            $wepay_account_id = $_REQUEST['wepay-account_id'];
        }

        if(empty($f_paypal)){
            $f_paypal = array(
                'mode' => $mode,
                'app_id' => $appid,
                'api_username' => $appusername,
                'api_password' => $apppassword,
                'api_signature' => $appsingature,
                'email' => $email,
                'mode_wepay' => $mode_wepay,
                'wepay-client_id' =>$wepay_client_id,
                'wepay-client_secret' =>$wepay_client_secret,
                'wepay-access_token' =>$wepay_access_token,
                'wepay-account_id' => $wepay_account_id
            );
            update_option('funding_paypal', $f_paypal);
        }
        $f_paypal = get_option('funding_paypal');
        define(
            'X_PAYPAL_API_BASE_ENDPOINT',
            $f_paypal['mode'] == 'sandbox' ? 'https://svcs.sandbox.paypal.com/' : 'https://svcs.paypal.com/'
        );
        // This is dirty, but the Paypal API likes constants
        define('SOCF_API_USERNAME', $f_paypal['api_username']);
        define('SOCF_API_PASSWORD', $f_paypal['api_password']);
        define('SOCF_API_SIGNATURE', $f_paypal['api_signature']);
        define('SOCF_APPLICATION_ID', $f_paypal['app_id']);
        // Some more PayPal settings
        define('X_PAYPAL_ADAPTIVE_SDK_VERSION','PHP_SOAP_SDK_V1.4_MODIFIED');
        define('X_PAYPAL_REQUEST_DATA_FORMAT','SOAP11');
        define('X_PAYPAL_RESPONSE_DATA_FORMAT','SOAP11');
        // Create project custom post type
        register_post_type('project',array(
            'label' => esc_html__('Projects', 'funding'),
            'taxonomies' => array('project-category', 'fundit_project'),
            'labels' => array(
                'name' => esc_html__('Projects', 'funding'),
                'singular_name' => esc_html__('Project', 'funding'),
                'add_new' => esc_html__('Create Project', 'funding'),
                'edit_item' => esc_html__('Edit Project', 'funding'),
                'add_new_item' => esc_html__('Add New Project', 'funding'),
                'edit_item' => esc_html__('Edit Project', 'funding'),
                'new_item' => esc_html__('New Project', 'funding'),
                'view_item' => esc_html__('View Project', 'funding'),
                'search_items' => esc_html__('Search Projects', 'funding'),
                'not_found' => esc_html__('No Projects Found', 'funding'),
            ),
            'description' => esc_html__('A fundable project.', 'funding'),
            'public' => true,
            '_builtin' =>  false,
            'supports' => array(
                'title',
                'editor',
                'author',
                'thumbnail',
                'excerpt',
                'comments',
                'revisions',
            ),
            'rewrite' => true,
            'query_var' => 'project',
            'menu_icon' => get_template_directory_uri().'/funding/admin/images/project.png',
        ));


        register_taxonomy(
            'project-category',
            'project',
            array(
                'label' => esc_html__( 'Categories', 'funding' ),
                'rewrite' => array( 'slug' => 'categories' ),
                'hierarchical' => true,
            )
        );

        register_taxonomy_for_object_type('tag', 'project');
        // Create reward custom post type
        register_post_type('reward',array(
            'label' => esc_html__('Reward', 'funding'),
            'description' => esc_html__('A reward for funding a project.', 'funding'),
            'public' => false,
        ));
        // Create funder custom post type
        register_post_type('funder',array(
            'label' => esc_html__('Funder', 'funding'),
            'description' => esc_html__('Funder of a project', 'funding'),
            'public' => false,
        ));
    }
    /**
     * Render the project page.
     */
    function action_template_redirect(){
        global $post;
        if(is_single() && $post->post_type == 'project'){
            $step = isset($_GET['step']) ? intval($_GET['step']) : 0;
            $project_settings = (array) get_post_meta($post->ID, 'settings', true);

			if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

			if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

             if (strpos( $project_settings['date'] , "/") !== false) {
  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
			}else{
				$parseddate = $project_settings['date'];
			}

            $project_expired = strtotime($parseddate) < time();
            global $f_currency_signs;
            $project_currency_sign = $f_currency_signs[$project_settings['currency']];
            $rewards = get_children(array(
                'post_parent' => $post->ID,
                'post_type' => 'reward',
                'order' => 'ASC',
                'orderby' => 'meta_value_num',
                'meta_key' => 'funding_amount',
            ));
            if(!empty($rewards)){
                $keys = array_keys($rewards);
                $lowest_reward = $keys[0];
                $funding_minimum = get_post_meta($lowest_reward, 'funding_amount', true);
            }
            // Get all funders
            $funders = array();
            $funded_amount = 0;
            $chosen_reward = null;
            foreach($rewards as $reward){
                $these_funders = get_children(array(
                    'post_parent' => $reward->ID,
                    'post_type' => 'funder',
                    'post_status' => 'publish'
                ));
                foreach($these_funders as $this_funder){
                    $funding_amount = get_post_meta($this_funder->ID, 'funding_amount', true);
                    $funders[] = $this_funder;
                    $funded_amount += $funding_amount;
                }
            }


            if (isset($_GET['preapproval_id'])) {
                if (is_numeric($_GET['preapproval_id'])) {

                    global $wpdb;
                    $results = $wpdb->get_results( "select post_id, meta_key from $wpdb->postmeta where meta_value = '".$_GET['preapproval_id']."'", ARRAY_A );
                    if (isset($results[0])) {
                        //well apparently he pledged
                        $theid = $results[0]['post_id'];//the founder ID with the wepay pledge
                        $funder = get_post($theid); //funder post
                        $reward = get_post($thepost->post_parent); //the reward post
                        $project = get_post($reward->post_parent); //the project post

                        $project_settings = (array) get_post_meta($project->ID, 'settings', true);
                        $notified = get_post_meta($funder->ID, 'notified', true);

                        global $f_currency_signs;
                        $project_currency_sign = $f_currency_signs[$project_settings['currency']];
                        if (empty($notified)){
                            // Email the funder and the author
                            $author = get_userdata($project->post_author);
                            $rewards = get_children(array(
                                'post_parent' => $project->ID,
                                'post_type' => 'reward',
                                'order' => 'ASC',
                                'orderby' => 'meta_value_num',
                                'meta_key' => 'funding_amount',
                            ));
                            $funders = array();
                            $funded_amount = 0;
                            $chosen_reward = null;
                            foreach($rewards as $this_reward){
                                $these_funders = get_children(array(
                                    'post_parent' => $this_reward->ID,
                                    'post_type' => 'funder',
                                    'post_status' => 'publish'
                                ));
                                foreach($these_funders as $this_funder){
                                    $funding_amount = get_post_meta($this_funder->ID, 'funding_amount', true);
                                    $funders[] = $this_funder;
                                    $funded_amount += $funding_amount;
                                }
                            }
                            $site = site_url();
                            $funder_details = get_post_meta($funder->ID, 'funder', true);
                            $funding_amount = get_post_meta($funder->ID, 'funding_amount', true);
                            $preapproval_key = get_post_meta($funder->ID, 'preapproval_key',true);
                            // Send an email to the post author

                            if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
								$array = explode('/', $project_settings['date']);
								$tmp = $array[0];
								$array[0] = $array[1];
								$array[1] = $tmp;
								unset($tmp);
								$project_settings['date'] = implode('/', $array);
							}

							if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				$project_settings['date'] = implode('/', $array);
			}

                            if (strpos( $project_settings['date'] , "/") !== false) {
				  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
							}else{
								$parseddate = $project_settings['date'];
							}
            				$project_expired = strtotime($parseddate) < time();

							if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){
							$timing = esc_html__('< 24', 'funding');
							}else{
                        	$timing = F_Controller::timesince(time(), strtotime($parseddate), 1, '');
							}


                         	if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){
                        	 $timingtext =  esc_html__('hours to go', 'funding');
                        	}else{

                        		if(F_Controller::timesince(time(), strtotime($parseddate), 1, '') == 1){
                        			$timingtext =  esc_html__('day to go', 'funding');
                        		}else{
                        			$timingtext = esc_html__('days to go', 'funding');
                        		}
	                       	}


                            $to_author = of_get_option('f2a');
                            $to_author = wordwrap(sprintf(
                                $to_author,
                                $author->user_nicename, // $author->display_name
                                ucfirst($funder_details['name']),
                                $project->post_title,
                                $project_currency_sign.$funded_amount,
                                round($funded_amount/$project_settings['target']*100),
                                $project_currency_sign.$project_settings['target'],
                                $timing. ' '. $timingtext,
                                $funder->ID,
                                $funder_details['name'],
                                $funder_details['email'],
                                $project_currency_sign.$funding_amount,
                                $preapproval_key,
                                $reward->post_title,
                                $funder->post_content
                            ), 75);
							$from_name = of_get_option("from_name");
							$from_address = of_get_option("from_adress");

							$headers = array('Content-Type: text/html; charset=UTF-8', 'From: "'.$from_name.'<'.$from_address.'>"\r\n"');
                            @wp_mail(
                                $author->user_email,
                                sprintf(esc_html__('New Funder For %s', 'funding'), $project->post_title),
                                $to_author,
                                $headers
                            );
                            // Send an email to the funder
                            $funder_paypal_email = get_post_meta($funder->ID, 'paypal_email', true);

                            $to_funder = of_get_option('f2f');
                            $to_funder = wordwrap(sprintf(
                                $to_funder,
                                $funder_details['name'],
                                $project->post_title,
                                round($funded_amount/$project_settings['target']*100),
                                $project_currency_sign.$project_settings['target'],
                                $timing. ' ' .$timingtext,
                                $funder->ID,
                                $funder_details['name'],
                                $funder_details['email'],
                                $funding_amount,
                                $preapproval_key,
                                $reward->post_title,
                                $funder->post_content,
                                get_permalink($project->ID),
                                get_bloginfo('name'),
                                site_url()
                            ),75);
							$from_name = of_get_option("from_name");
							$from_address = of_get_option("from_adress");
							$thanks = 	of_get_option("tff");
							$headers = array('Content-Type: text/html; charset=UTF-8', 'From: "'.$from_name.'<'.$from_address.'>"\r\n"');
                            @wp_mail(
                                $funder_paypal_email,
                                wordwrap(sprintf($thanks, $project->post_title)),
                                $to_funder,
                                $headers
                            );
                            update_post_meta($funder->ID, 'notified', true);
                        }
                        wp_publish_post( $theid );
                        $url = add_query_arg('thanks', 1, get_post_permalink($project->ID));
                        header("Location: ".$url, true, 303);

                    }
                }
            }
            // The chosen reward
            $reward = null;
            if(isset($_REQUEST['chosen_reward'])){
                $reward = get_post(intval($_REQUEST['chosen_reward']));
                $reward_funding_amount = get_post_meta($reward->ID, 'funding_amount', true);
                $reward_available = get_post_meta($reward->ID, 'available', true);
            }
            if($project_expired && $step > 0) {
                header('Location: '.get_permalink($post->ID), true, 301);
                exit();
            }

            if($step == 2){

                $name = $_REQUEST['name'];
                $mail = $_REQUEST['email'];
                $funders = get_posts(array(
                    'numberposts'     => -1,
                    'post_type' => 'funder',
                    'post_parent' => $reward->ID,
                    'post_status' => 'publish'
                ));
                $valid = false;
                $step = 1;
				global $f_paypal;
				$limit = $f_paypal['paypal_limit'];
				if(empty($limit))$limit = 99999999999999999999999;
				$project_currency_sign = $f_currency_signs[$project_settings['currency']];
                if(empty($name)){
                    $message = esc_html__('Please insert your name.', 'funding');
                }
                elseif(empty($mail)){
                    $message = esc_html__('Please insert your e-mail.', 'funding');
                }
				elseif(!is_email($mail)){
                    $message = esc_html__('Please insert valid e-mail.', 'funding');
                }
                elseif(empty($_REQUEST['amount'])){
                    $message = esc_html__('Please choose an amount.', 'funding');
                }
                elseif(floatval($_REQUEST['amount']) < $reward_funding_amount){
                    $message = esc_html__('You need to fund more for this reward.', 'funding');
                    $_REQUEST['amount'] = $reward_funding_amount;
                } elseif(floatval($_REQUEST['amount']) > $limit){
                    $message = esc_html__('Maximum funding amount is: ', 'funding').$project_currency_sign.$limit;
                }
                elseif(!empty($reward_available) && count($funders) >= $reward_available){
                    $message = esc_html__('The reward you chose is no longer available.', 'funding');
                } else{

                    $valid = true;
                    $step = 2;

                    // Create funder post
                    $funding_id = wp_insert_post(array(
                        'post_parent' => $reward->ID,
                        'post_type' => 'funder',
                        'post_status' => 'draft',
                        'post_content' => $_REQUEST['message'],
                    ));
                    add_post_meta($funding_id, 'funder', array(
                        'name' => $_REQUEST['name'],
                        'email' => $_REQUEST['email'],
                        'website' => $_REQUEST['website'],
                    ), true);
                    add_post_meta($funding_id, 'funding_amount', floatval($_REQUEST['amount']), true);

                    if ($_REQUEST['fundingmethod'] == "paypal") {
                        //if funding by paypal
                        // Redirect to PayPal
                        $paypal = new F_PayPal();

                        $funding = get_post($funding_id);

                        // Redirect
                        $url = $paypal->get_auth_url($post, $reward, $funding); ?>

    <script type="text/javascript">
    window.location.href='<?php echo $url; ?>';
    </script>
    <?php
                        add_post_meta($funding_id, 'funding_method', 'paypal' , true);
                        exit;
                    } else {
                        global $f_paypal;

                        //not funding by paypal, do the wepay
                        //$wepay = print_r ($_REQUEST, true);
                        require str_replace("tpl/", "", dirname(__FILE__).'/lib/WePay/wepay.php');
                        // application settings


                        $client_id = $f_paypal['wepay-client_id']; //api key for wepay
                        $client_secret = $f_paypal['wepay-client_secret']; //client secret for wepay
                        $access_token = $f_paypal['wepay-access_token'];
                        $amount = floatval($_REQUEST['amount']);


                        if ($f_paypal['wepay-staging'] != 'Yes')  {
                            // change to useProduction for live environments
                            Wepay::useProduction($client_id, $client_secret);
                        } else {
                            Wepay::useStaging($client_id, $client_secret);
                        }

						$user_ID1 = $post->post_author; //tusi
						$token = get_user_meta($user_ID1 ,"wepay_token", true);
                        $wepay = new WePay($token); // Don't pass an access_token for this call
                        $percent = $f_paypal['admin-commission']; //how much do we take
                        // create the pre-approval
                        $returnurl =  $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
                        if (strpos($returnurl, "http://") === false) {
                            $returnurl = "http://".$returnurl;
                        }

                        $totadminamount = $percent /100;
                        $app_fee = $totadminamount*$amount;

                         //if($app_fee < 1){
                          //  $app_fee = 1;
                          //  $amount = $amount - 1;
                        //}

						$account_id = get_user_meta($user_ID1 ,"wepay_account_id", true);
                        $response = $wepay->request('preapproval/create', array(
                            'account_id'         => $account_id,
   							'amount'            => $amount,
   							"currency" =>  	$project_settings['currency'],
   							'app_fee'          => $app_fee,
   							'fee_payer' => "payer",
                            'mode'              => 'regular',
                            'short_description' => esc_html__('Commit funding to project ', 'funding').$post->post_title,
                            'redirect_uri'      => $returnurl,
                            'period'            => 'once'
                        ));
                        add_post_meta($funding_id, 'funding_method', 'wepay' , true);
                        add_post_meta($funding_id, 'wepay_preapproval_id', $response->preapproval_id , true);


?>
    <script type="text/javascript">
        window.location.href='<?php echo esc_url($response->preapproval_uri); ?>';
    </script>
    <?php
                    exit;
                    }
                }
            }
            $templates = array(
                0 => 'f-project.php',
                1 => 'f-fund-project.php',
                2 => 'f-user-details.php',
            );
            $template = $templates[$step];
            $file = locate_template($template);
            if(empty($file)) $file = dirname(__FILE__).'/tpl/'.$template;
            // Include the CSS and Javascript
            if(file_exists(STYLESHEETPATH.'/f/f.css')) wp_enqueue_style('funding', get_stylesheet_directory_uri().'/f/f.css');
            elseif(file_exists(TEMPLATEPATH.'/f/f.css')) wp_enqueue_style('funding', get_template_directory_uri().'/f/f.css');
            else wp_enqueue_style('funding',get_template_directory_uri().'/funding/tpl/f.css');
            if(file_exists(STYLESHEETPATH.'/f/f.js')) wp_enqueue_script('funding', get_stylesheet_directory_uri().'/f/f.js', array('jquery'));
            elseif(file_exists(TEMPLATEPATH.'/f/f.js')) wp_enqueue_script('funding', get_template_directory_uri().'/f/f.js', array('jquery'));
            else wp_enqueue_script('funding', get_template_directory_uri().'/funding/tpl/f.js', array('jquery'));
            if($template == ""){include(dirname(__FILE__) .'/404.php');}else{
            include($file);}
            do_action('wp_shutdown');
            exit();
        }
    }
    /**
     * Handle IPN from PayPal
     */
    function method_paypal_ipn(){
        $this->method_funded();
    }
    /**
     * Handle a user returning from PayPal
     */
    function method_funded($funder_id = null){
        if(empty($funder_id)) $funder_id = intval($_REQUEST['funder_id']);
        $paypal = new F_PayPal();
        $funder = get_post($funder_id);
        // Check authentication and update the funder status
        $auth = $paypal->check_auth($funder);
        $reward = get_post($funder->post_parent);
        $project = get_post($reward->post_parent);
        $project_settings = (array) get_post_meta($project->ID, 'settings', true);
        $notified = get_post_meta($funder->ID, 'notified', true);
        global $f_currency_signs;
        $project_currency_sign = $f_currency_signs[$project_settings['currency']];
        if($auth && empty($notified)){
            // Email the  and the author
            $author = get_userdata($project->post_author);
            $rewards = get_children(array(
                'post_parent' => $project->ID,
                'post_type' => 'reward',
                'order' => 'ASC',
                'orderby' => 'meta_value_num',
                'meta_key' => 'funding_amount',
            ));
            $funders = array();
            $funded_amount = 0;
            $chosen_reward = null;
            foreach($rewards as $this_reward){
                $these_funders = get_children(array(
                    'post_parent' => $this_reward->ID,
                    'post_type' => 'funder',
                    'post_status' => 'publish'
                ));
                foreach($these_funders as $this_funder){
                    $funding_amount = get_post_meta($this_funder->ID, 'funding_amount', true);
                    $funders[] = $this_funder;
                    $funded_amount += $funding_amount;
                }
            }
            $site = site_url();
            $funder_details = get_post_meta($funder->ID, 'funder', true);
            $funding_amount = get_post_meta($funder->ID, 'funding_amount', true);
            $preapproval_key = get_post_meta($funder->ID, 'preapproval_key',true);

			if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				$project_settings['date'] = implode('/', $array);
			}

		if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				$project_settings['date'] = implode('/', $array);
			}

			 if (strpos( $project_settings['date'] , "/") !== false) {
  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
			}else{
				$parseddate = $project_settings['date'];
			}
            				$project_expired = strtotime($parseddate) < time();

							if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){
							$timing = esc_html__('< 24', 'funding');
							}else{
                        	$timing = F_Controller::timesince(time(), strtotime($parseddate), 1, '');
							}


                         	if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){
                        	 $timingtext =  esc_html__('hours to go', 'funding');
                        	}else{

                        		if(F_Controller::timesince(time(), strtotime($parseddate), 1, '') == 1){
                        			$timingtext =  esc_html__('day to go', 'funding');
                        		}else{
                        			$timingtext = esc_html__('days to go', 'funding');
                        		}
	                       	}
            // Send an email to the post author
            $to_author = of_get_option('f2a');
            $to_author = wordwrap(sprintf(
                $to_author,
                $author->user_nicename, // Shouldn't it be $author->display_name ?
                ucfirst($funder_details['name']),
                $project->post_title,
                $project_currency_sign.$funded_amount,
                round($funded_amount/$project_settings['target']*100),
                $project_currency_sign.$project_settings['target'],
                $timing. ' ' .$timingtext,
                $funder->ID,
                $funder_details['name'],
                $funder_details['email'],
                $project_currency_sign.$funding_amount,
                $preapproval_key,
                $reward->post_title,
                $funder->post_content
            ), 75);
			$from_name = of_get_option("from_name");
			$from_address = of_get_option("from_adress");

			$headers = array('Content-Type: text/html; charset=UTF-8', 'From: "'.$from_name.'<'.$from_address.'>"\r\n"');
            @wp_mail(
                $author->user_email,
                sprintf(esc_html__('New Funder For %s', 'funding'), $project->post_title),
                $to_author,
                $headers
            );
            // Send an email to the funder

            $funder_paypal_email = get_post_meta($funder->ID, 'paypal_email', true);
            $to_funder = of_get_option('f2f');
            $to_funder = wordwrap(sprintf(
                $to_funder,
                $funder_details['name'],
                $project->post_title,
                round($funded_amount/$project_settings['target']*100),
                $project_currency_sign.$project_settings['target'],
                $timing. ' ' .$timingtext,
                $funder->ID,
                $funder_details['name'],
                $funder_details['email'],
                $funding_amount,
                $preapproval_key,
                $reward->post_title,
                $funder->post_content,
                get_permalink($project->ID),
                get_bloginfo('name'),
                site_url()
            ),75);
			$from_name = of_get_option("from_name");
			$from_address = of_get_option("from_adress");
			$thanks = 	of_get_option("tff");
			$headers = array('Content-Type: text/html; charset=UTF-8', 'From: "'.$from_name.'<'.$from_address.'>"\r\n"');
            @wp_mail(
                $funder_paypal_email,
                wordwrap(sprintf($thanks, $project->post_title)),
                $to_funder,
                $headers
            );
            update_post_meta($funder->ID, 'notified', true);
        }
        $url = add_query_arg('thanks', 1, get_post_permalink($project->ID));
        header("Location: ".$url, true, 303);
    }
    ///////////////////////////////////////////////////////////////////
    // Support functions
    ///////////////////////////////////////////////////////////////////
    /**
    * Returns a string representation of the time between $time and $time2
    *
    * @param int $time A unix timestamp of the start time.
    * @param int $time2 A unix timestamp of the end time.
    * @param int $precision How many parts to include
    */
    static function timesince($time, $time2 = null, $precision = 2, $separator = ' '){
	    if(empty($time2)) $time2 = time();

	    $time_diff = $time2 - $time;
	    return ceil(abs($time2 - $time) / 86400);
	}
    static function get_funders($project_id){
        $rewards = get_children(array(
            'post_parent' => $project_id,
            'post_type' => 'reward',
            'order' => 'ASC',
            'orderby' => 'meta_value_num',
            'meta_key' => 'funding_amount',
        ));
        $funders = array();
        foreach($rewards as $this_reward){
            $these_funders = get_children(array(
                'post_parent' => $this_reward->ID,
                'post_type' => 'funder',
                'post_status' => 'publish'
            ));
            $funders = array_merge($funders, (array) $these_funders);
        }
        return $funders;
    }
}
if(!isset($class))$class='';
F_Controller::single($class);