<?php  /*
 * 	Template Name: Contact
 */
?>
<?php get_header(); ?>
          <?php
if(isset($_POST['submitted'])) {
	if(trim($_POST['contactName']) === '') {
		$nameError = esc_html__('Please enter your name.', 'funding');
		$hasError = true;
	} else {
		$name = trim($_POST['contactName']);
	}
	if(trim($_POST['email']) === '')  {
		$emailError = esc_html__('Please enter your email address.', 'funding');
		$hasError = true;
	} else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email']))) {
		$emailError = esc_html__('You entered an invalid email address.', 'funding');
		$hasError = true;
	} else {
		$email = trim($_POST['email']);
	}
	if(trim($_POST['comments']) === '') {
		$commentError = esc_html__('Please enter a message.', 'funding');
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($_POST['comments']));
		} else {
			$comments = trim($_POST['comments']);
		}
	}
	if(!isset($hasError)) {
		$emailTo = of_get_option('contact_email');
		if (!isset($emailTo) || ($emailTo == '') ){
			$emailTo = of_get_option('contact_email');
		}
        $sub = $_POST['subject'];
		$subject = esc_html__('[PHP Snippets] From ','funding').$name;
		$body = "Name: $name \n\nEmail: $email \n\nSubject: $sub \n\nComments: $comments";
		wp_mail($emailTo, $subject, $body);
		$emailSent = true;
	}
} ?>
<?php
             if(of_get_option('contact_map_enable') == 1){
            ?>
            <div class="gmap<?php if ( of_get_option('fullwidth') ) { }else{ ?> normal-page container boxed<?php } ?> ">
                 <div id="map-canvas"></div>
            </div>
            <?php
             }
            ?>
            
<div class="container page cpage">
    <div class="row">
        
 
	<div class="contact span12">
  
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				  <!--<h3 class="title"><span><?php esc_html_e("Contact form", 'funding'); ?></span></h3> -->
					<div class="entry-content">
						<?php if(isset($emailSent) && $emailSent == true) { ?>
							<div class="thanks">
								<p><?php esc_html_e("Thanks, your email was sent successfully", 'funding'); ?>.</p>
							</div>
						<?php } else { ?>
							<?php the_content(); ?>
							<?php if(isset($hasError) || isset($captchaError)) { ?>
								<p class="error"><?php esc_html_e("Sorry, an error occured.", 'funding'); ?><p>
							<?php } ?>
				<?php } ?>
				</div><!-- .entry-content -->
				<?php endwhile; endif; ?>
		</div><!-- #contact -->
        </div>
         <!-- /.span8 -->
</div>
<?php get_footer(); ?>