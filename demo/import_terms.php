<?php
$wpdb->query("DROP TABLE IF EXISTS {$table_prefix}terms");
$wpdb->query("CREATE TABLE {$table_prefix}terms (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8");
$wpdb->query("
INSERT INTO {$table_prefix}terms
VALUES 
('1', 'Uncategorized', 'uncategorized', '0'),
('2', 'Games', 'games', '0'),
('3', 'Technology', 'technology', '0'),
('4', 'Comics', 'comics', '0'),
('5', 'Music', 'music', '0'),
('6', 'Photography', 'photography', '0'),
('7', 'Art', 'art', '0'),
('8', 'Film &amp; Video', 'film-video', '0'),
('9', 'Menu 1', 'menu-1', '0'),
('10', 'blog', 'blog', '0'),
('11', 'Festival', 'festival', '0'),
('12', 'Important', 'important', '0'),
('13', 'Money', 'money', '0'),
('14', 'Concert', 'concert', '0'),
('15', 'Updates', 'updates', '0'),
('16', 'Weekend', 'weekend', '0'),
('17', 'Meeting', 'meeting', '0'),
('18', 'Talk', 'talk', '0')");
?>