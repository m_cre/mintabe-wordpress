<?php
$wpdb->query("DROP TABLE IF EXISTS {$table_prefix}term_taxonomy");
$wpdb->query("CREATE TABLE {$table_prefix}term_taxonomy (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8");
$wpdb->query("
INSERT INTO {$table_prefix}term_taxonomy
VALUES 
('1', '1', 'category', '', '0', '0'),
('2', '2', 'project-category', '', '0', '5'),
('3', '3', 'project-category', '', '0', '3'),
('4', '4', 'project-category', '', '0', '5'),
('5', '5', 'project-category', '', '0', '4'),
('6', '6', 'project-category', '', '0', '3'),
('7', '7', 'project-category', '', '0', '3'),
('8', '8', 'project-category', '', '0', '3'),
('9', '9', 'nav_menu', '', '0', '19'),
('10', '10', 'category', '', '0', '6'),
('11', '11', 'post_tag', '', '0', '3'),
('12', '12', 'post_tag', '', '0', '6'),
('13', '13', 'post_tag', '', '0', '2'),
('14', '14', 'post_tag', '', '0', '4'),
('15', '15', 'post_tag', '', '0', '4'),
('16', '16', 'post_tag', '', '0', '3'),
('17', '17', 'post_tag', '', '0', '2'),
('18', '18', 'post_tag', '', '0', '3')");
?>