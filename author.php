<?php get_header(); ?>

<?php
global $wp_query;

$author_id = $wp_query->query_vars['author'];
?>

<div class="row profile">
	<div class="container">
		<div class="profile-info row">

			<div class="span3">
				<div class="shadow">

					<?php
					$autorpic = get_the_author_meta( 'profile_pic', $author_id );

					if ( ! empty( $autorpic ) ) {
					?>

						<?php
						$image = aq_resize( $autorpic,  250, 250, true, true, true ); //resize & crop img
						if ( ! isset ( $image[0] ) ) {
							$theimage = $autorpic;
						} else {
							$theimage = $image;
						}
						?>

						<img src="<?php echo $theimage; ?>" />

					<?php } else { ?>

						<?php echo get_avatar( $author_id, 250 ); ?>

					<?php } ?>

				</div>
			</div>


			<div class="tabbable span9"> <!-- Only required for left/right tabs -->
				<div class="tab-content">
					<div id="profile" class="tab-pane active">

						<div class="span10">

							<h1>
								<?php
								if ( $display_name = get_the_author_meta( 'display_name', $author_id ) ) {
									echo $display_name;
								}
								?>

								<?php if ( usercountry_name_display( $author_id ) != "" ) { ?>
									<small><i class="fa fa-map-marker"></i><?php echo get_the_author_meta( 'city', $author_id ); ?>, <?php echo usercountry_name_display( $author_id ); ?></small>
								<?php } ?>
							</h1>

							<?php  if ( $description = get_the_author_meta('description', $author_id ) ) { ?>
								<div class="biography"><p><?php echo $description; ?></p></div>
							<?php } ?>


							<table>

								<?php
								if ( get_the_author_meta( 'first_name', $author_id ) ) { ?>
									<tr>
										<td>
											<i class="fa fa-user"></i> &nbsp;<?php esc_html_e( 'Name', 'funding' ); ?>
										</td>
										<td>
											<?php
											echo get_the_author_meta( 'first_name', $author_id );

											if ( $last_name = get_the_author_meta( 'last_name', $author_id ) ) {
												echo ' ';
												echo $last_name;
											}
											?>
										</td>
									</tr>
								<?php } ?>

								<?php
								if ( get_the_author_meta( 'user_registered', $author_id ) ) { ?>
									<tr>
										<td>
											<i class="fa fa-calendar"></i> &nbsp;<?php esc_html_e( 'Member Since', 'funding' ); ?>
										</td>
										<td>
											<?php echo date( 'F Y', strtotime( get_userdata( $author_id )->user_registered ) ); ?>
										</td>
									</tr>
								<?php } ?>

								<?php
								if ( $user_url = get_the_author_meta( 'user_url', $author_id ) ) { ?>
									<tr>
										<td>
											<i class="fa fa-globe"></i> &nbsp;<?php esc_html_e("Website", 'funding'); ?>
										</td>
										<td>
											<a target="_blank" href="<?php echo $user_url; ?>">
												<?php echo $user_url; ?>
											</a>
										</td>
									</tr>
								<?php } ?>

							</table>

						</div>
						<!-- end .span10 -->

					</div>
					<!-- end #profile -->

				</div>
				<!-- end .tab-content -->

			</div>
			<!-- end .tabbable -->

		</div>
		<!-- end .profile-info -->

	</div>
	<!-- end .container -->

</div>
<!-- end .profile -->


<div class="profile-projects">

	<div class="container blog">
		<div class="row">

			<h2><?php esc_html_e( 'Projects', 'funding'); ?></h2>

			<div class="span12 isoprblck">
		        <?php

				$args = array(
					'post_type'      => 'project',
					'order'          => 'ASC',
					'author'         => $author_id,
					'posts_per_page' => -1
				);

				$wp_query = new WP_Query( $args );

				if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

					<?php
					global $post;
					global $f_currency_signs;

					$author_id             = get_the_author_meta( 'ID' );
					$project_settings      = (array) get_post_meta( $post->ID, 'settings', true );

				if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}


					if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}


						if (strpos( $project_settings['date'] , "/") !== false) {
			  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
						}else{
							$parseddate = $project_settings['date'];
						}
            		$project_expired = strtotime($parseddate) < time();
					$project_currency_sign = $f_currency_signs[$project_settings['currency']];
					$target                = $project_settings['target'];
					$rewards               = get_children( array(
						'post_parent' => $post->ID,
						'post_type' => 'reward',
						'order' => 'ASC',
						'orderby' => 'meta_value_num',
						'meta_key' => 'funding_amount',
					));
					$funders       = array();
					$funded_amount = 0;
					$chosen_reward = null;

					foreach( $rewards as $this_reward ) {
						$these_funders = get_children( array(
							'post_parent' => $this_reward->ID,
							'post_type'   => 'funder',
							'post_status' => 'publish'
						));
						foreach( $these_funders as $this_funder ) {
							$funding_amount = get_post_meta( $this_funder->ID, 'funding_amount', true );
							$funders[]      = $this_funder;
							$funded_amount  += $funding_amount;
						}
					}
					?>

					<?php if ( empty( $target ) or $target == 0 ) { $target = 1; } ?>

					<div class="project-card span3">
						<?php if ( has_post_thumbnail() ) { ?>

							<?php
							$thumb   = get_post_thumbnail_id();
							$img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
							$image   = aq_resize( $img_url, 311, 210, true, '', true ); //resize & crop img
							?>

							<div class="project-thumb-wrapper">
								<a href="<?php the_permalink(); ?>"><img src="<?php echo $image[0]; ?>" /></a>
							</div>

						<?php } else { ?>
							<div class="project-thumb-wrapper">
								<a href="<?php the_permalink(); ?>"><img class="pbimage" src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/defaults/default_project.jpg"></a>
							</div>
						<?php } ?>

						<h5 class="bbcard_name">
							<a href="<?php the_permalink(); ?>"><?php $title = get_the_title(); echo mb_substr( $title, 0, 23 ); if ( strlen( $title ) > 23 ) { echo '...'; } ?></a>
						</h5>

						<?php if(get_the_author_meta('first_name',$author_id) or get_the_author_meta('last_name',$author_id)){ ?><span><?php esc_html_e("by", 'funding'); ?> <a href="<?php echo esc_url(get_author_posts_url($author_id)); ?>"><?php echo esc_attr(get_the_author_meta('first_name',$author_id).' '.get_the_author_meta('last_name',$author_id)); ?></a></span> <?php } ?>

						<p>
							<?php
							$excerpt = get_the_excerpt();
							echo mb_substr( $excerpt, 0, 110 );
							echo '...';
							?>
						</p>

						<p>
							<?php if ( usercountry_name_display( $author_id ) != '' || get_the_author_meta( 'city', $author_id ) != '' ) { ?>
								<span class="icon-map-marker" ></span><b><?php echo esc_attr( usercountry_name_display( $author_id ) ); ?></b>
								<?php if ( get_the_author_meta( 'city', $author_id ) != '' ) { echo ', '; } ?>
								<?php if ( get_the_author_meta( 'city', $author_id ) ) { echo get_the_author_meta( 'city', $author_id ); } ?>
							<?php } ?>
						</p>

						<?php if ( $funded_amount == $target or $funded_amount > $target ) { ?>
							<div class="project-successful">
								<strong><?php esc_html_e( 'Successful!', 'funding' ) ?></strong>
							</div>
						<?php } elseif ( $project_expired ) { ?>
							<div class="project-unsuccessful">
								<strong><?php esc_html_e( 'Unsuccessful!', 'funding' ) ?></strong>
							</div>
						<?php } else { ?>
							<div class="progress progress-striped active bar-green">
								<div style="width: <?php printf( esc_html__( '%u%', 'funding' ), round( $funded_amount/$target*100 ), $project_currency_sign, round( $target ) ) ?>%" class="bar"></div>
							</div>
						<?php } ?>

						<ul class="project-stats">
							<li class="first funded">
								<strong><?php printf( esc_html__( '%u%%', 'funding' ), round( $funded_amount/$target*100 ), $project_currency_sign, number_format( round( (int) $target ), 0, '.', ',') ); ?></strong><?php esc_html_e( 'funded', 'funding' ); ?>
							</li>
							<li class="pledged">
								<strong>
								<?php print $project_currency_sign; print number_format( round( (int) $target ), 0, '.', ',' ); ?>
								</strong>
								<?php esc_html_e('target', 'funding'); ?>
							</li>
							<li data-end_time="2013-02-24T08:41:18Z" class="last ksr_page_timer">
								  <?php
			                    if(!$project_expired) : ?>
			                        <?php if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){ ?> <strong> <?php esc_html_e('< 24', 'funding'); ?></strong> <?php }else{ ?>
			                        <strong><?php print F_Controller::timesince(time(), strtotime($parseddate), 1, ''); } ?></strong>
			                        <?php if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){ ?>
			                         <?php esc_html_e('hours to go', 'funding'); ?>
			                        <?php }else{ ?>
			                        	<?php if(F_Controller::timesince(time(), strtotime($parseddate), 1, '') == 1){ ?>
			                        		 <?php esc_html_e('day to go', 'funding'); ?>
			                        	<?php }else{ ?>
			                        		 <?php esc_html_e('days to go', 'funding'); ?>
			                        	<?php } ?>


			                        <?php } ?>
			                    <?php endif; ?>
							</li>
						</ul>

						<div class="clear"></div>
					</div>

				<?php endwhile; endif; ?>

				<div class="clear"></div>

			</div>
			<!-- end .span12 -->

		</div>
		<!-- end .row -->

	</div>
	<!-- end .container -->

</div>
<!-- end .profile -->


<?php get_footer(); ?>