<?php
/** A simple text block **/
class Page_Header_Block extends Block {
    //set and create block
    function __construct() {
        $block_options = array(
            'name' => esc_html__('Page Header', 'funding'),
            'size' => 'span12',
            'title2' => '',
        );
        //create the block
        parent::__construct('page_header_block', $block_options);
    }
    function form($instance) {
        $defaults = array(
            'text' => '',
        );
        $instance = wp_parse_args($instance, $defaults);
        extract($instance);
        ?>
        <p class="description">
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">
                <?php esc_html_e("Title", 'funding'); ?>
                <?php echo field_input('title', $block_id, $title, $size = 'full') ?>
            </label>
        </p>
        <p class="description">
            <label for="<?php echo esc_attr($this->get_field_id('title2')); ?>">
                <?php esc_html_e("Subtitle", 'funding'); ?>
                <?php echo field_input('title2', $block_id, $title2, $size = 'full') ?>
            </label>
        </p>
        <?php
    }
    function pbblock($instance) {
        extract($instance);
		echo '<div class="span12 block-title centered">
			<h2>'.esc_attr(strip_tags($title)).'</h2>
			<p>'.esc_attr(strip_tags($title2)).'</p>
		</div><div class="span12 block-divider"></div>';
    }
}