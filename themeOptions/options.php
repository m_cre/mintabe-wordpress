<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */
function optionsframework_option_name() {
    // This gets the theme name from the stylesheet (lowercase and without spaces)
    $themename = wp_get_theme();
    $themename = $themename['Name'];
    $themename = preg_replace("/\W/", "", strtolower($themename) );
    $optionsframework_settings = get_option('optionsframework');
    $optionsframework_settings['id'] = $themename;
    update_option('optionsframework', $optionsframework_settings);
}
function optionsframework_options() {
    // Slider Options
    $slider_choice_array = array("none" => "No Showcase", "accordion" => "Accordion", "wpheader" => "WordPress Header", "image" => "Your Image", "easing" => "Easing Slider", "custom" => "Custom Slider");
    // Pull all the categories into an array
    $options_categories = array();
    $options_categories_obj = get_categories();
    foreach ($options_categories_obj as $category) {
        $options_categories[$category->cat_ID] = $category->cat_name;
    }
    // Pull all the pages into an array
    $options_pages = array();
    $options_pages_obj = get_pages('sort_column=post_parent,menu_order');
    $options_pages[''] = 'Select a page:';
    foreach ($options_pages_obj as $page) {
        $options_pages[$page->ID] = $page->post_title;
    }
    // If using image radio buttons, define a directory path
    $radioimagepath =  get_stylesheet_directory_uri() . '/themeOptions/images/';
    // define sample image directory path
    $imagepath =  get_template_directory_uri() . '/images/demo/';
    $options = array();
    $options[] = array( "name" => esc_html__("General  Settings",'funding'),
                        "type" => "heading");
	$options[] = array( "name" => esc_html__("General  Settings", 'funding'),
                        "type" => "info");

    $options[] = array( "name" => esc_html__("Upload Your Logo",'funding'),
                        "desc" => esc_html__("Upload your logo. I recommend keeping it within reasonable size. Max 150px and minimum height of 90px but not more than 120px.",'funding'),
                        "id" => "logo",
                        "std" => get_template_directory_uri()."/img/logo.jpg",
                        "type" => "upload");
    $options[] = array( "name" => esc_html__("Jquery Scrollbar", 'funding'),
                        "desc" => esc_html__("Enable the option of a smooth jquery scrollbar.", 'funding'),
                        "id" => "scrollbar",
                        "std" => "1",
                        "type" => "jqueryselect");
	 $options[] = array( "name" => __("Share this", 'funding'),
                        "desc" => __("Disable Share this plguin.", 'funding'),
                        "id" => "share_this",
                        "std" => "0",
                        "type" => "jqueryselect");
    $options[] = array( "name" => esc_html__("Blog settings",'funding'),
                        "type" => "info");
    $options[] = array( "name" => esc_html__("Blog category",'funding'),
                        "desc" => esc_html__("Insert ID of blog category. You can find it in Posts -> Categories.",'funding'),
                        "id" => "blogcat",
                        "std" => "",
                        "type" => "text");

//social settings
       $options[] = array( "name" => esc_html__("Social Settings", 'funding'), "type" => "heading");
  $options[] = array( "name" => esc_html__("Social Settings - Facebook", 'funding'),
                        "type" => "info");
      $options[] = array( "name" => esc_html__("Turn on Facebook", 'funding'),
                        "desc" => esc_html__("Use Facebook for social login", 'funding'),
                        "id" => "facebook_btn",
                        "std" => "0",
                        "type" => "jqueryselect");
    $options[] = array( "name" => esc_html__("Facebook App ID", 'funding'),
                        "desc" => esc_html__("Add your Facebook App ID here", 'funding'),
                        "id" => "facebook_app",
                        "std" => "Facebook app ID",
                        "type" => "text");
    $options[] = array( "name" => esc_html__("Facebook App Secret", 'funding'),
                        "desc" => esc_html__("Add your Facebook App Secret here", 'funding'),
                        "id" => "facebook_secret",
                        "std" => "Facebook Secret",
                        "type" => "text");

      $options[] = array( "name" => esc_html__("Social Settings - Twitter", 'funding'),
                        "type" => "info");
     $options[] = array( "name" => esc_html__("Turn on Twitter", 'funding'),
                        "desc" => esc_html__("Use Twitter for social login", 'funding'),
                        "id" => "twitter_btn",
                        "std" => "0",
                        "type" => "jqueryselect");
    $options[] = array( "name" => esc_html__("Twitter App ID", 'funding'),
                        "desc" => esc_html__("Add your Twitter API Key here", 'funding'),
                        "id" => "twitter_app",
                        "std" => "Twitter API key",
                        "type" => "text");
    $options[] = array( "name" => esc_html__("Twitter API Secret", 'funding'),
                        "desc" => esc_html__("Add your Twitter API Secret here", 'funding'),
                        "id" => "twitter_secret",
                        "std" => "Twitter Secret",
                        "type" => "text");

      $options[] = array( "name" => esc_html__("Social Settings - Google+", 'funding'),
                        "type" => "info");
     $options[] = array( "name" => esc_html__("Turn on Google+", 'funding'),
                        "desc" => esc_html__("Use Google+ for social login", 'funding'),
                        "id" => "google_btn",
                        "std" => "0",
                        "type" => "jqueryselect");
    $options[] = array( "name" => esc_html__("Google+ App ID", 'funding'),
                        "desc" => esc_html__("Add your Google+ API Key here", 'funding'),
                        "id" => "google_app",
                        "std" => "Google+ API key",
                        "type" => "text");
    $options[] = array( "name" => esc_html__("Google+ API Secret", 'funding'),
                        "desc" => esc_html__("Add your Google+ API Secret here", 'funding'),
                        "id" => "google_secret",
                        "std" => "google+ Secret",
                        "type" => "text");

// Funding section
    $options[] = array( "name" => esc_html__("Funding",'funding'),
                        "type" => "heading");
	$options[] = array( "name" => esc_html__("Funding Settings", 'funding'),
                        "type" => "info");
    $options[] = array( "name" => esc_html__("Add Text",'funding'),
                        "desc" => esc_html__("Enter the important text on the commit to funding page.",'funding'),
                        "id" => "important_text",
                        "type" => "textarea");
/* $options[] = array( "name" => esc_html__("Enable collect fundings",'funding'),
                        "desc" => esc_html__("Enable users to collect fundings before project ends.",'funding'),
                        "id" => "colfun",
                        "std" => "0",
                        "type" => "jqueryselect");
			*/

$options[] = array( "name" => esc_html__("Use reward system",'funding'),
                        "desc" => esc_html__("Enable users to collect rewards for funding projects.",'funding'),
                        "id" => "rewards",
                        "std" => "1",
                        "type" => "jqueryselect");


$options[] = array( "name" => esc_html__("Use PayPal system",'funding'),
                        "desc" => esc_html__("Enable users to fund projects using PayPal.",'funding'),
                        "id" => "paypal",
                        "std" => "1",
                        "type" => "jqueryselect");


$options[] = array( "name" => esc_html__("Use WePay system",'funding'),
                        "desc" => esc_html__("Enable users to fund projects using WePay.",'funding'),
                        "id" => "wepay",
                        "std" => "1",
                        "type" => "jqueryselect");

$options[] = array( "name" => esc_html__("Auto publish projects",'funding'),
                        "desc" => esc_html__("Allow automatic publishing of projects",'funding'),
                        "id" => "autopr",
                        "std" => "0",
                        "type" => "jqueryselect");
	$options[] = array( "name" => esc_html__("Email Settings", 'funding'),
                        "type" => "info");
$allowed_tags = array(
	'br' => array(),
);
$options[] = array( "name" => esc_html__("Email from name",'funding'),
                        "desc" => esc_html__("Add name for from part of email header",'funding'),
                        "id" => "from_name",
                        "std" => "Fundingpress",
                        "type" => "text");
$options[] = array( "name" => esc_html__("Email from address",'funding'),
                        "desc" => esc_html__("Add email address for from part of email header",'funding'),
                        "id" => "from_adress",
                        "std" => 'fundingpress@fundingpress.com',
                        "type" => "text");

$options[] = array( "name" => esc_html__("Thanks for funding email template",'funding'),
                        "desc" => esc_html__("Thanks for funding email template.",'funding'),
                        "id" => "tff",
                        "std" => wp_kses(__("Thanks For Funding %s", 'funding'), $allowed_tags ),
                        "type" => "textarea");


$options[] = array( "name" => esc_html__("Funded to project author, email template",'funding'),
                        "desc" => esc_html__("Email template that will be sent to project author.",'funding'),
                        "id" => "f2a",
                        "std" => wp_kses(__("
Hi %s<br/>
Oh yeah! %s has just commited to funding your project - %s.<br/>
<br/>
With this contribution, you've raised %s (%u%%) of your %s target. You still have %s.<br/>
<br/>
Funder ID:				%u<br/>
Name:					%s<br/>
Email:					%s<br/>
Amount:					%s<br/>
Preapproval Key:		%s<br/>
Reward:					%s<br/>
Message:				%s<br/>
<br/>
Don't forget to email them and say thanks. That'll encourage them to share your project with their friends and followers.<br/>
<br/>
Cheers!", 'funding'), $allowed_tags ),
                        "type" => "textarea");


$options[] = array( "name" => esc_html__("Funded to funder, email template",'funding'),
                        "desc" => esc_html__("Email template that will be sent to funder.",'funding'),
                        "id" => "f2f",
                        "std" => wp_kses(__("
Hi %s<br/>
<br/>
Thanks for funding our project - %s. You've helped us reach %u%% of our %s target with %s to go. For your records, here are your details:<br/>
<br/>
Funder ID:				%u<br/>
Name:					%s<br/>
Email:					%s<br/>
Amount:					%s<br/>
Preapproval Key:		%s<br/>
Reward:					%s<br/>
Message:				%s<br/>
<br/>
If we reach our target, we'll contact you with details about your reward. Please share this project with your friends and followers. We need your help to reach our target.<br/>
<br/>
%s<br/>
Thanks,<br/>
%s<br/>
%s", 'funding'), $allowed_tags ),
                        "type" => "textarea");
	/*
	$options[] = array( "name" => esc_html__("WePay Settings", 'funding'),
                        "type" => "info");
$options[] = array( "name" => esc_html__("Enable WePay staging",'funding'),
                        "desc" => esc_html__("Use WePay staging?",'funding'),
                        "id" => "wepay_staging",
                        "std" => "1",
                        "type" => "jqueryselect");
    $options[] = array( "name" => esc_html__("WePay Client ID",'funding'),
                        "desc" => esc_html__("Paste your WePay client ID here",'funding'),
                        "id" => "wepay_client_id",
                        "std" => "",
                        "type" => "text");
    $options[] = array( "name" => esc_html__("WePay Client Secret",'funding'),
                        "desc" => esc_html__("Paste your WePay client secret here",'funding'),
                        "id" => "wepay_client_secret",
                        "std" => "",
                        "type" => "text");
     $options[] = array( "name" => esc_html__("WePay Token",'funding'),
                        "desc" => esc_html__("Paste your WePay token here",'funding'),
                        "id" => "wepay_token",
                        "std" => "",
                        "type" => "text");

*/

// Social Media
    $options[] = array( "name" => esc_html__("Social Media",'funding'),
                        "type" => "heading");
	$options[] = array( "name" => esc_html__("Social Media", 'funding'),
                        "type" => "info");
// Social Network setup
    /*$options[] = array( "name" => "Facebook App ID",
                        "desc" => "Add your Facebook App ID here",
                        "id" => "facebook_app",
                        "std" => "1234567890",
                        "type" => "text");
*/
    $options[] = array( "name" => esc_html__("Enable Twitter",'funding'),
                        "desc" => esc_html__("Show or hide the Twitter icon that shows on the header section.",'funding'),
                        "id" => "twitter",
                        "std" => "0",
                        "type" => "jqueryselect");
    $options[] = array( "name" => esc_html__("Twitter Link",'funding'),
                        "desc" => esc_html__("Paste your twitter link here.",'funding'),
                        "id" => "twitter_link",
                        "std" => "#",
                        "type" => "text");
    $options[] = array( "name" => esc_html__("Enable Facebook",'funding'),
                        "desc" => esc_html__("Show or hide the Facebook icon that shows on the header section.",'funding'),
                        "id" => "facebook",
                        "std" => "0",
                        "type" => "jqueryselect");
    $options[] = array( "name" => esc_html__("Facebook Link",'funding'),
                        "desc" => esc_html__("Paste your facebook link here.",'funding'),
                        "id" => "facebook_link",
                        "std" => "#",
                        "type" => "text");
    $options[] = array( "name" => esc_html__("Enable Google+",'funding'),
                        "desc" => esc_html__("Show or hide the Google+ icon that shows on the header section.",'funding'),
                        "id" => "googleplus",
                        "std" => "0",
                        "type" => "jqueryselect");
    $options[] = array( "name" => esc_html__("Google+ Link",'funding'),
                        "desc" => esc_html__("Paste your google+ link here.",'funding'),
                        "id" => "google_link",
                        "std" => "#",
                        "type" => "text");
    $options[] = array( "name" => esc_html__("Enable skype",'funding'),
                        "desc" => esc_html__("Show or hide the skype icon that shows on the header section.",'funding'),
                        "id" => "skype",
                        "std" => "0",
                        "type" => "jqueryselect");
    $options[] = array( "name" => esc_html__("Skype name",'funding'),
                        "desc" => esc_html__("Paste your skype name here.",'funding'),
                        "id" => "skype_name",
                        "std" => "#",
                        "type" => "text");
    $options[] = array( "name" => esc_html__("Enable RSS",'funding'),
                        "desc" => esc_html__("Show or hide the RSS icon that shows on the header section.",'funding'),
                        "id" => "rss",
                        "std" => "0",
                        "type" => "jqueryselect");
    $options[] = array( "name" => esc_html__("RSS Link",'funding'),
                        "desc" => esc_html__("Paste your RSS link here.",'funding'),
                        "id" => "rss_link",
                        "std" => "#",
                        "type" => "text");
// Colour Settings
    $options[] = array( "name" => esc_html__("Colours",'funding'),
                        "type" => "heading");
	$options[] = array( "name" => esc_html__("Site colours", 'funding'),
                        "type" => "info");
    $options[] = array( "name" => esc_html__("Primary Color",'funding'),
    "desc" => esc_html__("The primary color for the site.",'funding'),
    "id" => "primary_color",
    "std" => "#76cc1e",
    "type" => "color" );
$options[] = array( "name" => esc_html__("Button colors",'funding'),
                        "type" => "info");
    //regular
    $options[] = array(
    "name" => esc_html__("Button color",'funding'),
    "desc" => esc_html__("Button color <a class='button-medium'  >Example</a>.",'funding'),
    "id" => "button_green",
    "std" => "#76cc1e",
    "type" => "color");
     $options[] = array(
    "name" => esc_html__("Button hover color",'funding'),
    "desc" => esc_html__("Button hover color",'funding'),
    "id" => "button_hover",
    "std" => "#689c06",
    "type" => "color");
    //border
    $options[] = array(
    "name" => esc_html__("Button border color",'funding'),
    "desc" => esc_html__("Color for button border <a class='button-medium'  >Example</a>.",'funding'),
    "id" => "button_border",
    "std" => "#689c06",
    "type" => "color");

// Footer section start
    $options[] = array( "name" => esc_html__("Footer",'funding'), "type" => "heading");
	$options[] = array( "name" => esc_html__("Footer", 'funding'),
                        "type" => "info");
                $options[] = array( "name" => esc_html__("Copyright",'funding'),
                        "desc" => esc_html__("Enter your copyright text.",'funding'),
                        "id" => "copyright",
                        "std" => esc_html__("Made by Skywarrior Themes.",'funding'),
                        "type" => "textarea");

                $options[] = array( "name" => esc_html__("Privacy link",'funding'),
                        "desc" => esc_html__("Enter your privacy link. Please include http://",'funding'),
                        "id" => "privacy",
                        "std" => "http://www.skywarriorthemes.com/",
                        "type" => "text");
                $options[] = array( "name" => esc_html__("Terms link",'funding'),
                        "desc" => esc_html__("Enter your terms link. Please include http://",'funding'),
                        "id" => "terms",
                        "std" => "http://www.skywarriorthemes.com/",
                        "type" => "text");
// contact page code
$options[] = array( "name" => esc_html__("Contact",'funding'),
                        "type" => "heading");
	$options[] = array( "name" => esc_html__("Contact", 'funding'),
                        "type" => "info");
    $options[] = array( "name" => esc_html__("Enter admin email address ",'funding'),
                        "desc" => esc_html__(" Enter your email address.",'funding'),
                        "id" => "contact_email",
                        "std" => "admin@gmail.com",
                        "type" => "text");

	$options[] = array( "name" => esc_html__("Gmap settings", 'funding'),
                        "type" => "info");
    $options[] =      array( "name" => "Turn Map on/off",
                "desc" => "Show Map on Contact us page.",
                "id" => "contact_map_enable",
                "type" => "select",
                "std" => "1",
                 "type" => "jqueryselect");
    $options[] = array( "name" => esc_html__("Upload Your Marker Logo For Map", 'funding'),
                        "desc" => esc_html__("We recommend keeping it within reasonable size.", 'funding'),
                        "id" => "contact_marker_logo",
                        "std" => get_template_directory_uri()."/img/marker.png",
                        "type" => "upload");
    $options[] = array( "name" => esc_html__("Enter location Latitude", 'funding'),
                        "desc" => esc_html__("Enter location Latitude.", 'funding'),
                        "id" => "maplat",
                        "std" => "51.508742",
                        "type" => "text");
     $options[] = array( "name" => esc_html__("Enter location Longitude", 'funding'),
                        "desc" => esc_html__("Enter location Longitude.", 'funding'),
                        "id" => "maplon",
                        "std" => "-0.125656",
                        "type" => "text");
// end contact page code

	$options[] = array( "name" => esc_html__("One click install", 'funding'),
                        "type" => "heading");

	$options[] = array( "name" => esc_html__("demo install", 'funding'),
                        "desc" => esc_html__("Click to install pre-inserted demo contents.", 'funding'),
                        "id" => "demo_install",
                        "std" => "0",
                        "type" => "impbutton");

    return $options;
}
?>