<?php
/*
 * Template name: Home page
 */
?>
<?php get_header(); ?>

            <div class="container page" role="main">
                <div class="row">
                    <div class="span12">
			<?php $pattern = get_shortcode_regex();
			preg_match('/'.$pattern.'/s', $post->post_content, $matches);
			if (is_array($matches) && $matches[2] == 'layerslider') {
			   $shortcode = $matches[0];
			   echo do_shortcode($shortcode);
			} ?>
                <?php while ( have_posts() ) : the_post(); ?>

                    <?php add_shortcode( 'layerslider', '__return_false' ); the_content(); ?>

                    <?php get_template_part( 'content', 'page' ); ?>


                <?php endwhile; // end of the loop. ?>

                    </div><!-- #span12 -->
                </div><!-- #row -->
            </div><!-- #container -->



<?php get_footer(); ?>