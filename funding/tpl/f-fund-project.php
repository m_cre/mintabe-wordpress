<?php

/*
 * This is the default template for rendering a funding page.
 *
 * The user chooses the amount they want to fund and the reward they'd like.
 */

?>

<?php get_header(); the_post(); ?>

<div class="container blog">
	<div id="primary">
		<div id="content" role="main">

			<?php if(!empty($message)) : ?>
				<div id="form-error-message"><?php print esc_attr($message); ?></div>
			<?php endif; ?>

			<form id="funding-form" method="post" action="<?php print add_query_arg(array('step' => 2), get_post_permalink()) ?>">
				<div class="span7">
								<h3 style="margin-top:0px;"><?php esc_html_e('How much would you like to contribute?', 'funding'); ?></h3>
				<ul id="project-rewards-list">
					<li>
						<span><?php echo esc_attr($project_currency_sign); ?></span>
						<input type="text" name="amount" id="field-amount" value="<?php esc_html_e(@$_REQUEST['amount']) ?>" />
						<div class="clear"></div>
					</li>
				</ul>
            <div class="rewardnasubmitu1">
				<h3><?php esc_html_e('Choose Your Reward', 'funding'); ?></h3>
				<ul id="project-rewards-list" class="perks-wrapper">
					<?php foreach($rewards as $reward) : ?>
						<?php
							$reward_funding_amount = get_post_meta($reward->ID, 'funding_amount', true);
							$reward_available = get_post_meta($reward->ID, 'available', true);
							$funders = get_posts(array(
								'numberposts'     => -1,
								'post_type' => 'funder',
								'post_parent' => $reward->ID,
								'post_status' => 'publish'
							));
						?>

						<?php if(empty($reward_available) || count($funders) < $reward_available) : ?>
							<li class="perk">
								<label for="<?php print 'reward-'.$reward->ID ?>">
									<input class="chosen_reward" type="radio" name="chosen_reward" value="<?php print $reward->ID ?>" id="<?php print 'reward-'.$reward->ID ?>" <?php checked($reward->ID, @$_REQUEST['chosen_reward']) ?> />
									<input type="hidden" name="chosen_reward_value" value="<?php print $reward_funding_amount; ?>" id="reward_funding_amount" />
									<div class="funding-perk-content">
										<h5><?php print $reward->post_title ?></h5>
										<?php if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') { ?>
                                           <div class="min-amount"><strong><?php printf(esc_html__('Pledge %s%s or more', 'funding'), $project_currency_sign, number_format($reward_funding_amount, 2));?></strong></div>
                                        <?php } else { ?>
                                           <div class="min-amount"><strong><?php printf(esc_html__('Pledge %s%s or more', 'funding'), $project_currency_sign, money_format('%.2n', $reward_funding_amount));?></strong></div>
                                        <?php } ?>
										<p><?php print $reward->post_content ?></p>
									</div>
									<div class="clear"></div>
								</label>
							</li>
						<?php endif; ?>

					<?php endforeach ?>
				</ul>
				</div>
				<div class="who-are-you">
					<h3><?php esc_html_e('Who Are You?', 'funding'); ?></h3>
					<dl>
						<lh><label for="field-name"><?php esc_html_e('Your Name', 'funding') ?></label></lh>
						<dt><input type="text" name="name" id="field-name" value="<?php esc_html_e(@$_REQUEST['name']) ?>" /></dt>

						<lh><label for="field-email"><?php esc_html_e('Your Email', 'funding') ?></label></lh>
						<dt><input type="text" name="email" id="field-email" value="<?php esc_html_e(@$_REQUEST['email']) ?>" /></dt>

					</dl>
				</div>
				<div class="funding-method">
				<h3><?php esc_html_e('Choose a funding method', 'funding'); ?></h3>
					<dl>
						<lh><label for="fundingmethod"><?php esc_html_e('Please select your funding method', 'funding') ?></label></lh>
						 <?php if(of_get_option('paypal') == '1'){ ?>
						<input type="radio" name="fundingmethod" value="paypal" id="funding-paypal" <?php if (isset($_REQUEST['fundingmethod'])) {if ($_REQUEST['fundingmethod'] == "paypal") {echo "checked";}} else { echo "checked"; } ?>/><?php esc_html_e('PayPal', 'funding') ?></input>
						<?php } ?>
						 <?php if(of_get_option('wepay') == '1'){ ?>
						<input type="radio" name="fundingmethod" value="wepay" id="funding-wepay"  <?php if (isset($_REQUEST['fundingmethod'])) {if ($_REQUEST['fundingmethod'] == "wepay") {echo "checked";}} ?>/><?php esc_html_e('WePay', 'funding') ?></input>
						<?php } ?>

					</dl>
				</div>
				<div class="clear"></div>
				</div>
				<div class="span4">

					<h3 style="margin-top:0px; margin-bottom:15px;"><?php esc_html_e('You are helping fund:', 'funding'); ?></h3>

					<div class="project-card span3">
                      <?php if(has_post_thumbnail()){
                    $thumb = get_post_thumbnail_id();
                    $img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
                    $image = aq_resize( $img_url, 311, 210, true, '', true ); //resize & crop img
                ?>
              <div class="project-thumb-wrapper"><a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url($image[0]); ?>" /></a></div>
                <?php
                }else{ ?>
                <div class="project-thumb-wrapper"><a href="<?php the_permalink(); ?>"><img class="pbimage" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/defaults/default_project.jpg"></a></div>
                <?php } ?>
                       <h5 class="bbcard_name"><a href="<?php the_permalink(); ?>"><?php $title = get_the_title(); echo esc_attr(mb_substr($title, 0,20)); if(strlen($title) > 23){echo '...';}?></a></h5>
			<?php if(get_the_author_meta('first_name',get_the_author_meta('ID')) or get_the_author_meta('last_name',get_the_author_meta('ID'))){ ?><span><?php esc_html_e("by", 'funding'); ?> <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php echo esc_attr(get_the_author_meta('first_name',get_the_author_meta('ID')).' '.get_the_author_meta('last_name',get_the_author_meta('ID'))); ?></a></span> <?php } ?>
            <p> <?php
                $excerpt = get_the_excerpt();
                echo mb_substr($excerpt, 0,80);echo '...';
             ?></p>

            <?php
                global $post;
                global $f_currency_signs;
                $project_settings = (array) get_post_meta($post->ID, 'settings', true);


				if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

				if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}



               	if (strpos( $project_settings['date'] , "/") !== false) {
  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
			}else{
				$parseddate = $project_settings['date'];
			}

	            $project_expired = strtotime($parseddate) < time();
                $project_currency_sign = $f_currency_signs[$project_settings['currency']];
                $target= $project_settings['target'];

                $rewards = get_children(array(
                'post_parent' => $post->ID,
                'post_type' => 'reward',

                'order' => 'ASC',
                'orderby' => 'meta_value_num',
                'meta_key' => 'funding_amount',
            ));

            $funders = array();
            $funded_amount = 0;
            $chosen_reward = null;

            foreach($rewards as $this_reward){
                $these_funders = get_children(array(
                    'post_parent' => $this_reward->ID,
                    'post_type' => 'funder',
                    'post_status' => 'publish'
                ));
                foreach($these_funders as $this_funder){
                    $funding_amount = get_post_meta($this_funder->ID, 'funding_amount', true);
                    $funders[] = $this_funder;
                    $funded_amount += $funding_amount;
                }
            }?>
                 <?php if($funded_amount == $target or $funded_amount > $target){ ?>
                <div class="project-successful">
                    <strong><?php esc_html_e('Successful!', 'funding') ?></strong>
                </div>
            <?php }elseif($project_expired){ ?>
                        <div class="project-unsuccessful">
                            <strong><?php esc_html_e('Unsuccessful!', 'funding') ?></strong>
                        </div>
            <?php }else{ ?>
            <div class="progress progress-striped active bar-green"><div style="width: <?php printf(esc_html__('%u%', 'funding'), round($funded_amount/$target*100), $project_currency_sign, round($target)) ?>%" class="bar"></div></div>
            <?php } ?>
            <ul class="project-stats">
                <li class="first funded">
                     <strong><?php printf(esc_html__('%u%%', 'funding'), round($funded_amount/$target*100), $project_currency_sign, number_format(round((int)$target), 0, '.', ',')) ?></strong><?php esc_html_e('funded', 'funding'); ?>
                </li>
                <li class="pledged">
                    <strong>
                        <?php print $project_currency_sign; print number_format(round((int)$target), 0, '.', ',');?></strong><?php esc_html_e('target', 'funding'); ?>
                </li>
                <li data-end_time="2013-02-24T08:41:18Z" class="last ksr_page_timer">
                    <?php

                    if(!$project_expired) : ?>
                        <?php if(strpos(F_Controller::timesince(time(), strtotime($project_settings['date']), 1, ''), "hour")){echo 0;}else{ ?>
                        <strong><?php print F_Controller::timesince(time(), strtotime($project_settings['date']), 1, ''); } ?></strong>
                        <?php esc_html_e('days to go', 'funding'); ?>

                    <?php endif; ?>
                </li>
            </ul>
            <div class="clear"></div>
            </div>
                <?php  if (of_get_option('important_text')!=""){ ?>
			       <div class="notice">
					<h6 class="important"><span class="highlight"><?php esc_html_e('Important', 'funding'); ?></span></h6>

                    <?php echo of_get_option('important_text'); ?>

					</div>
                    <?php } ?>

                    <?php if(of_get_option('paypal') == '1'){ ?>
			       <a href="#" style="float:left;" onclick="javascript:window.open('https://www.paypal.com/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');"><img  src="<?php echo esc_url(get_template_directory_uri()); ?>/img/paypal_payment.jpg" border="0" alt="Solution Graphics"></a>
					<?php } ?>

					<?php if(of_get_option('wepay') == '1'){ ?>
					<a href="#" style="float:left;" onclick="javascript:window.open('https://www.wepay.com/about');"><img  src="<?php echo esc_url(get_template_directory_uri()); ?>/img/WePay_Logo.png" border="0" alt="WePay"></a>
					<?php } ?>

				<div class="clear"></div>
				</div>
				<div class="clear"></div>
				<h3 class="funding-comments-title"><?php esc_html_e('Comment', 'funding'); ?></h3>
				<p><?php esc_html_e('If you selected a reward that involve received an item, please write the address in the box bellow. You can also use it if you want to tell something to the creator of the project.', 'funding'); ?></p>
				<ul id="funding-comments">
					<li>
						<textarea name="message"><?php esc_html_e(@$_REQUEST['message']) ?></textarea>
					</li>
				</ul>


				 <?php if(of_get_option('paypal') == '1' or of_get_option('wepay') == '1'){ ?>
				<div class="submit">
					<input type="submit" class="button-green button-medium button-contribute" value="<?php esc_html_e('Commit To Funding', 'funding') ?>" />
				</div>
				<?php } ?>
				<div id="funding-information">
					<?php include(dirname(__FILE__).'/info.php') ?>
				</div>


			</form>

		</div><!-- #content -->
	</div><!-- #primary -->
</div>
<?php get_footer() ?>