<?php
if(session_save_path() != '/tmp'){
session_save_path('/tmp');}
@session_start();
// store session data
/** A latest posts for categories block **/
class Project_Highlight_Block extends Block {
    //set and create block
    function __construct() {
        $block_options = array(
            'name' => esc_html__('Project highlight', 'funding'),
            'size' => 'span12',
        );
        //create the block
        parent::__construct('project_highlight_block', $block_options);
    }
    function form($instance) {
        $defaults = array(
            'text' => '',
        );
        $line_options = array(
            'latest' => esc_html__('Latest projects', 'funding'),
            'staff' => esc_html__('Staff picks', 'funding'),
            'featured' => esc_html__('Featured campaign', 'funding'),
            'successful' => esc_html__('Latest successful projects', 'funding'),
            'ending' => esc_html__('First ending projects', 'funding'),
        );
        $instance = wp_parse_args($instance, $defaults);
        extract($instance);
            $args = array(
            'type'                     => 'project',
            'child_of'                 => 0,
            'parent'                   => '',
            'orderby'                  => 'name',
            'order'                    => 'ASC',
            'hide_empty'               => 1,
            'hierarchical'             => 1,
            'exclude'                  => '',
            'include'                  => '',
            'number'                   => '',
            'taxonomy'                 => 'project-category',
            'pad_counts'               => false

        );

         $categories = get_categories( $args );
        foreach ($categories as $cat) {
            $cats[$cat->cat_ID] = $cat->cat_name;
        }
        ?>
        <p class="description">
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">
                <?php esc_html_e("Title (optional)", 'funding'); ?>
                <?php echo field_input('title', $block_id, $title, $size = 'full') ?>
            </label>
        </p>
       <p class="description">
            <label for="<?php echo esc_attr($this->get_field_id('newscats')); ?>">
                <?php esc_html_e("Check categories you want to include", 'funding'); ?><br />
                <?php echo field_checkboxfromarray('newscats', $block_id, $cats, $instance['newscats']) ?>
            </label>
        </p>

        <p class="description fourth">
            <label for="<?php echo esc_attr($this->get_field_id('display')); ?>">
                <?php esc_html_e("Pick display option", 'funding'); ?><br/>
                <?php echo field_select('display', $block_id, $line_options, $display, $block_id); ?>
            </label>
        </p>
        <?php
    }
    function pbblock($instance) {
        extract($instance);
            if (is_array($newscats)) {
                $myArray = $newscats;
                foreach ($myArray as &$value)$value;
                $value = implode(',',  $myArray);;
            } else {
                $value="";
            }

        if($display == 'latest'){$_SESSION['displ'] = 1; }elseif($display == 'staff'){$_SESSION['displ'] = 2;}elseif($display == 'featured'){$_SESSION['displ'] = 3;}elseif($display == 'successful'){$_SESSION['displ'] = 4;}elseif($display == 'ending'){$_SESSION['displ'] = 5;}else{$_SESSION['displ'] = 6;}
       switch($display) {
            case 'latest': //////////////////////////////////////////latest
            /*  list_categories();*/
        if($title == ""){}else{?><div class="title"><h4><?php echo esc_attr($title); ?></h4></div><?php }
            $args = array(
              'type' => 'project',
              'taxonomy' => 'project-category',
              'orderby' => 'name',
              'order' => 'ASC',
              'include' => $value
              );

            $categories = get_categories($args);?>
            <ul id="category-menu">
                <?php foreach ( $categories as $cat ) {?>
                <li id="cat-<?php echo esc_attr($cat->term_id); ?>"><a id="click"  class="<?php echo esc_attr($cat->slug); ?> ajax" onclick="cat_ajax_get('<?php echo esc_attr($cat->term_id); ?>');" ><?php echo esc_attr($cat->name); ?></a></li>
                <?php } ?>
            </ul>
            <div id="category-post-content"></div>
            <div id="loading-animation">
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/loading.gif"/>
            </div>



                <?php
                break;
            case 'staff': //////////////////////////////////staff
             /* list_categories();*/
if($title == ""){}else{?><div class="title"><h4><?php echo esc_attr($title); ?></h4></div><?php }
            $args=array(
              'type' => 'project',
              'taxonomy' => 'project-category',
              'orderby' => 'name',
              'order' => 'ASC',
              'include' => $value
              );
            $categories = get_categories($args);

			 ?>
             <div id="category-post-content"></div>
             <div id="loading-animation">
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/loading.gif"/>
             </div>
             <ul id="category-menu">
                <?php foreach ( $categories as $cat ) {
                       global $post;
                       $term = get_term( $cat->term_id, 'project-category' );

                       $args = array (
                           'post_type' => 'project',
                           'orderby' => 'post_date',
                           'project-category' => $term->slug);
                            $posts = get_posts( $args );

                           foreach ( $posts as $post ) {
                                   global $post;
                               setup_postdata($post);

          if(get_post_meta($post->ID, '_smartmeta_staff-check-field', true) == 'true'){ ?>
          <li id="cat-<?php echo esc_attr($cat->term_id); ?>"><a id="click" class="<?php echo esc_attr($cat->slug); ?> ajax" onclick="cat_ajax_get('<?php echo esc_attr($cat->term_id); ?>');" ><?php echo esc_attr($cat->name); ?></a></li>
          <?php   break; }}}?>
            </ul>
            <?php
                break;
     case 'featured': //////////////////////////////////featured
             /* list_categories();*/
if($title == ""){}else{?><div class="title"><h4><?php echo esc_attr($title); ?></h4></div><?php }
            $args=array(
              'type' => 'project',
              'taxonomy' => 'project-category',
              'orderby' => 'name',
              'order' => 'ASC',
              'include' => $value
              );
            $categories = get_categories($args); ?>
             <div id="category-post-content"></div>
             <div id="loading-animation">
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/loading.gif"/>
             </div>
             <ul id="category-menu">
                <?php foreach ( $categories as $cat ) {
                       global $post;
                       $term = get_term( $cat->term_id, 'project-category' );

                       $args = array (
                           'showposts' => 1,
                           'post_type' => 'project',
                           'orderby' => 'post_date',
                           'project-category' => $term->slug);
                            $posts = get_posts( $args );

                           foreach ( $posts as $post ) {
                                   global $post;
                               setup_postdata($post);
          if(get_post_meta($post->ID, '_smartmeta_featured', true) == 'true'){ ?>
          <li id="cat-<?php echo esc_attr($cat->term_id); ?>"><a id="click" class="<?php echo esc_attr($cat->slug); ?> ajax" onclick="cat_ajax_get('<?php echo esc_attr($cat->term_id); ?>');" ><?php echo esc_attr($cat->name); ?></a></li>
          <?php   }}}?>
            </ul>
            <?php
                break;
              case 'successful': ////////////////////////////////////////successful
            /*  list_categories();*/
if($title == ""){}else{?><div class="title"><h4><?php echo esc_attr($title); ?></h4></div><?php }
             $args = array(
              'type' => 'project',
              'taxonomy' => 'project-category',
              'orderby' => 'name',
              'order' => 'ASC',
              'include' => $value
              );
            $categories = get_categories($args);

            ?>
            <div id="category-post-content"></div>
            <div id="loading-animation">
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/loading.gif"/>
            </div>
            <ul id="category-menu">
                <?php foreach ( $categories as $cat ) {
                       global $post;
                       $term = get_term( $cat->term_id, 'project-category' );

                       $args = array (
                           'showposts' => -1,
                           'post_type' => 'project',
                           'orderby' => 'post_date',
                           'project-category' => $term->name);
                            $posts = get_posts( $args );

                           foreach ( $posts as $post ) {
                                 global $post;
                                 setup_postdata($post);
                                global $f_currency_signs;
                        $project_settings = (array) get_post_meta($post->ID, 'settings', true);

						if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

						if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

							if (strpos( $project_settings['date'] , "/") !== false) {
			  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
						}else{
							$parseddate = $project_settings['date'];
						}
                        $project_expired = strtotime($project_settings['date']) < time();
                        $project_currency_sign = $f_currency_signs[$project_settings['currency']];
                        $target= $project_settings['target'];
                        $rewards = get_children(array(
                        'post_parent' => $post->ID,
                        'post_type' => 'reward',
                        'order' => 'ASC',
                        'orderby' => 'meta_value_num',
                        'meta_key' => 'funding_amount',
                    ));
                    $funders = array();
                    $funded_amount = 0;
                    $chosen_reward = null;
                    foreach($rewards as $this_reward){
                        $these_funders = get_children(array(
                            'post_parent' => $this_reward->ID,
                            'post_type' => 'funder',
                            'post_status' => 'publish'
                        ));
                        foreach($these_funders as $this_funder){
                            $funding_amount = get_post_meta($this_funder->ID, 'funding_amount', true);
                            $funders[] = $this_funder;
                            $funded_amount += $funding_amount;
                        }
                    }
                  if(empty($target) or $target == 0){$target = 1;}
                  if($funded_amount == $target or $funded_amount > $target){ ?>

                     <li id="cat-<?php echo esc_attr($cat->term_id); ?>"><a id="click" class="<?php echo esc_attr($cat->slug); ?> ajax" onclick="cat_ajax_get('<?php echo esc_attr($cat->term_id); ?>');" ><?php echo esc_attr($cat->name); ?></a></li>

          <?php break;  }else{}}}?>
            </ul>

    <?php   break;
            case 'ending'://///////////////////////////////////ending
         /*  list_categories();*/
if($title == ""){}else{?><div class="title"><h4><?php echo esc_attr($title); ?></h4></div><?php }
              $args = array(
              'type' => 'project',
              'taxonomy' => 'project-category',
              'orderby' => 'name',
              'order' => 'ASC',
              'include' => $value
              );
            $categories = get_categories($args); ?>
            <div id="category-post-content"></div>
            <div id="loading-animation">
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/loading.gif"/>
            </div>
            <ul id="category-menu">
                <?php foreach ( $categories as $cat ) { ?>
                <li id="cat-<?php echo esc_attr($cat->term_id); ?>"><a id="click" class="<?php echo esc_attr($cat->slug); ?> ajax" onclick="cat_ajax_get('<?php echo esc_attr($cat->term_id); ?>');" ><?php echo esc_attr($cat->name); ?></a></li>
                <?php } ?>
            </ul>
            <?php
                break;
        }
    }
}