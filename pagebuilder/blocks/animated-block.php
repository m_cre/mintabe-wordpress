<?php
/** A simple text block **/
class Animated_Block extends Block {
    //set and create block
    function __construct() {
        $block_options = array(
            'name' => esc_html__('Animated block', 'funding'),
            'size' => 'span3',
        );
        //create the block
        parent::__construct('animated_block', $block_options);
    }
    function form($instance) {
        $defaults = array(
            'title' => '',
            'delay' => '',
            'animated_image' => '',
            'type' => '',
            'marg' => ''

        );
         $type_options = array(
                'fade-in-from-left' => esc_html__('Fade in from left', 'funding'),
                'fade-in-from-right' => esc_html__('Fade in from right', 'funding'),
                'fade-in-from-bottom' => esc_html__('Fade in from bottom', 'funding'),
                'fade-in' => esc_html__('Fade in', 'funding'),
                'grow-in' => esc_html__('Grow in', 'funding')
            );
        $instance = wp_parse_args($instance, $defaults);
        extract($instance);
        ?>
        <p class="description">
            <label for="<?php echo $this->get_field_id('title') ?>">
               <?php esc_html_e("Title (optional)", 'funding'); ?>
                <?php echo field_input('title', $block_id, $title, $size = 'full') ?>
            </label>
        </p>
         <p class="description">
            <label for="<?php echo $this->get_field_id('image') ?>">
                <?php esc_html_e("Upload image", 'funding') ?>
                <?php echo field_upload("animated_image",$block_id,$animated_image,'image');?>
            </label>
        </p>
        <p class="description">
            <label for="<?php echo $this->get_field_id('delay') ?>">
               <?php esc_html_e("Data delay: (1-1000)", 'funding'); ?>
               <?php echo field_input('delay', $block_id, $delay, $size = 'full') ?>
            </label>
        </p>
        <p class="description half">
                <label for="<?php echo $this->get_field_id('type') ?>">
                    <?php esc_html_e("Animation type", 'funding') ?><br/>
                    <?php echo field_select('type', $block_id, $type_options, $type) ?>
                </label>
            </p>
        <p class="description">
            <label for="<?php echo $this->get_field_id('marg') ?>">
             <?php esc_html_e("Remove bottom spacing &nbsp;&nbsp;", 'funding'); ?>
                <?php echo field_checkbox('marg', $block_id, $marg, $check = 'true') ?>
            </label>
        </p>
        <?php
    }
    function pbblock($instance) {
        extract($instance);
        if($title) echo '<h3 class="widget-title">'.strip_tags($title).'</h3>';
        if($marg){
            echo '<div class="animated-no-margin">';
            echo '<img alt="" src="'.$animated_image.'" data-animation="'.$type.'" data-delay="'.$delay.'" class="img-with-animation" >';
            echo '</div>';
        }else{
            echo '<img alt="" src="'.$animated_image.'" data-animation="'.$type.'" data-delay="'.$delay.'" class="img-with-animation" >';
        }
     }
}