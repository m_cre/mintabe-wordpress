<?php
/**
 * Page Builder Config
 *
 * This file handles various configurations
 * of the page builder page
 *
 */
function page_builder_config() {
	$config = array(); //initialise array
	/* Page Config */
	$config['menu_title'] = esc_html__('Page Builder', 'funding');
	$config['page_title'] = esc_html__('Page Builder', 'funding');
	$config['page_slug'] ='page_builder';
	/* This holds the contextual help text - the more info, the better.
	 * HTML is of course allowed in all of its glory! */
	$config['contextual_help'] =
		'<p>' . esc_html__('The page builder allows you to create custom page templates which you can later use for your pages.', 'funding') . '<p>' .
		'<p>' . esc_html__('To use the page builder, start by adding a new template. You can drag and drop the blocks on the left into the building area on the right of the screen. Each block has its own unique configuration which you can manually configure to suit your needs', 'funding') . '<p>' .
		'<p>' . esc_html__('Please refer to the', 'funding') . '<a href="" target="_blank" alt="Page Builder Documentation">'. esc_html__(' documentation page ', 'funding') . '</a>'. esc_html__('for more information on how to use this feature.', 'funding') . '<p>';
	/* Debugging */
	$config['debug'] = false;
	return $config;
}