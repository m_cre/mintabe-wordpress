</div> <!-- End of container -->
  <!-- FOOTER -->
    <footer>
      <div class="container">

           <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer widgets') ) : ?>
         		<?php dynamic_sidebar('one'); ?>
           <?php endif; ?>

      </div>
    </footer>

    <div class="copyright">
    	<div class="container  cpr">
          	<p>© <?php echo date("Y"); ?>&nbsp;<?php if(of_get_option('copyright')!=""){ echo of_get_option('copyright');} ?>
        		&nbsp;
        	<a href="<?php if(of_get_option('privacy')!=""){echo of_get_option('privacy');}?>"><?php esc_html_e("Privacy", 'funding'); ?></a> ·
        	<a href="<?php if(of_get_option('terms')!=""){echo of_get_option('terms');}?>"><?php esc_html_e("Terms", 'funding'); ?></a></p>
        </div>
    </div>
<?php if(of_get_option('share_this')){ ?>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "ur-a91f8d30-251f-5433-c4d5-a5f68bb6664", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<?php } ?>

<!--Google map -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script>
var map = jQuery("#map-canvas");
if(map.length !== 0){
        function initialize() {
  var mapOptions = {
    zoom: 12,
    scrollwheel: false,
    center: new google.maps.LatLng("<?php echo of_get_option('maplat');?>", "<?php echo of_get_option('maplon');?>"),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  var contentString = 'Hello!';
  var infowindow = new google.maps.InfoWindow({
      content: contentString
  });
  var image = "<?php echo of_get_option('contact_marker_logo');?>";
  var myLatLng = new google.maps.LatLng("<?php echo of_get_option('maplat');?>", "<?php echo of_get_option('maplon');?>");
  var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: image
  });
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });
}
google.maps.event.addDomListener(window, 'load', initialize);
}
</script>

<?php  echo of_get_option('googlean'); ?>


<script>
	function social_startlogin(provider, proceed) {
		var CurrentLocation = "<?php
		if (is_single()) {
			echo wp_get_shortlink();
		} else {
			echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		}

		?>";
		window.location.replace(settings.authlocation+"handler/index.php?initiatelogin=" + provider + "&returnto=" + encodeURIComponent(CurrentLocation));
	}
	<?php
	if (isset($_SESSION['needtorefresh'])) {
		if ($_SESSION['needtorefresh'] == true) {
			unset ($_SESSION['needtorefresh']);
			echo 'setTimeout(function(){
				   window.location.reload(1);
				}, 1000);';
		}
	}

	?>


    function cat_ajax_get(catID) {

        jQuery('#category-menu li').click(function(li) {
        jQuery('li').removeClass('current');
        jQuery(this).addClass('current');
        });

     jQuery("#category-post-content").hide();
    jQuery("#loading-animation").show();


       jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {"action": "load-filter", cat: catID },
        success: function(response) {
            jQuery("#category-post-content").html(response);
            jQuery("#loading-animation").hide();
               jQuery("#category-post-content").show();
            return false;
        }
    });
}


  function get_currency_sign(cur) {


       jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {"action": "return_currency", curr: cur },
        success: function(response) {
              jQuery( ".pb-target .cu" ).text(response);
            return false;
        }
    });
}

function cat_ajax_get_all(catID) {
       jQuery('#category-menu li').click(function(li) {
        jQuery('li').removeClass('current');
        jQuery(this).addClass('current');
        });

     jQuery("#category-post-content").hide();
    jQuery("#loading-animation").show();



       jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {"action": "load-filter-all", cat: catID },
        success: function(response) {
            jQuery("#category-post-content").html(response);
            jQuery("#loading-animation").hide();
            jQuery("#category-post-content").show();
            return false;
        }
    });
}
</script>

<script>
jQuery(document).ready(function() {
	jQuery('#WePayUnlink').click(function (e) {

       	jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {"action": "unlink_wepay"},
        success: function(response) {
           if (response.substr(0,2) == "ok") {
           		location.reload();
           }
        }
    });
	});








    jQuery( "#currency" )
  .change(function () {
    var str = "";
    jQuery( "#currency option:selected" ).each(function() {
      str += jQuery( this ).val();
    });
    jQuery( ".pb-target .cu" ).text("<?php if(isset($f_currency_signs['EUR'])){ echo $f_currency_signs['EUR'];}?>");
  })
  .change(); });
</script>

<?php if(of_get_option("rewards") == 1){ ?>
<script>
/*rewards options*/
jQuery(document).ready(function() {
var chosen = jQuery('.chosen_reward');
var amount = jQuery('#field-amount');
var zerorew = jQuery('.chosen_reward_value');
var rewlist = jQuery('#project-rewards-list');
if(amount.val() == 0 ){
	jQuery('#project-rewards-list .chosen_reward').first().prop('checked', true);
}
 amount.keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


chosen.click(function() {
    amount.val(jQuery(this).next('input').val());
});
});
</script>
<?php }else{ ?>
  <script>jQuery( document ).ready(function() {
            jQuery('.chosen_reward').prop('checked', true);
           });
   </script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/rewards.css">
<?php } ?>
<script>
function delpost(id){


        jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            data: {  action: 'delcomments',id: id},
            success: function(data, textStatus, XMLHttpRequest){
               jQuery('#li-comment-'+id).hide();
               jQuery('#li-comment-update-'+id).hide();
            },
            error: function(){
               console.log('failure');
            }
        });
}


function delete_project(prID) {

       	jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {"action": "delete_projects", "idp" : prID},
        success: function(response) {
           if (response.substr(0,2) == "ok") {
           		location.reload();
           }
        }
    });
}

</script>
<?php wp_footer(); ?>
</div>
</body></html>