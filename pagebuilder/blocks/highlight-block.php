<?php
/** A simple text block **/
class Highlight_Block extends Block {
    //set and create block
    function __construct() {
        $block_options = array(
            'name' => esc_html__('Highlight', 'funding'),
            'size' => 'span12',
            'htext' => '',
            'hbutton' => '',
            'hlink' => '',
            'marg' => '',
        );
        //create the block
        parent::__construct('highlight_block', $block_options);
    }
    function form($instance) {
        $defaults = array(
            'text' => '',
        );
        $instance = wp_parse_args($instance, $defaults);
        extract($instance);
        ?>
        <p class="description">
            <label for="<?php echo esc_attr($this->get_field_id('htext')); ?>">
               <?php esc_html_e("Highlight text" , 'funding') ?>
                <?php echo field_input('htext', $block_id, $htext, $size = 'full') ?>
            </label>
        </p>
        <p class="description">
            <label for="<?php echo esc_attr($this->get_field_id('hbutton')); ?>">
               <?php esc_html_e("Highlight button text" , 'funding') ?>
                <?php echo field_input('hbutton', $block_id, $hbutton, $size = 'full') ?>
            </label>
        </p>
         <p class="description">
            <label for="<?php echo esc_attr($this->get_field_id('hlink')); ?>">
               <?php esc_html_e("Highlight button link" , 'funding') ?>
                <?php echo field_input('hlink', $block_id, $hlink, $size = 'full') ?>
            </label>
        </p>
           <p class="description">
            <label for="<?php echo esc_attr($this->get_field_id('marg')); ?>">
            <?php esc_html_e("Remove bottom spacing &nbsp;&nbsp;", 'funding') ?>
                <?php echo field_checkbox('marg', $block_id, $marg, $check = 'true') ?>
            </label>
        </p>
        <?php
    }
    function pbblock($instance) {
        extract($instance);
        if($marg){
         echo '<div class="highlight-no-margin highlight bgpattern span12">';
        }else{
           echo '<div class="highlight bgpattern span12">';
        }
       echo '<div class="container">
            <h2>'.esc_attr(strip_tags($htext)).'</h2>
            <a href="'.esc_url(strip_tags($hlink)).'" class="button-medium pull-right">'.esc_attr(strip_tags($hbutton)).'</a>
        </div>
		</div>
        ';
    }
}