<?php

/** A latest posts for projects block **/
class Projects_Block extends Block {


    //set and create block
    function __construct() {
        $block_options = array(
            'name' => esc_html__('Projects', 'funding'),
            'size' => 'span12',
        );

        //create the block
        parent::__construct('projects_block', $block_options);
    }

    function form($instance) {

        $defaults = array(
            'text' => '',
            'newscats' => '',
             'bslide' => '',
             'bstatic'=> ''
        );

        $line_options = array(
            'static' => esc_html__('Static projects', 'funding'),
            'slide' => esc_html__('Slider projects', 'funding'),
            'all' => esc_html__('Show both', 'funding')
         );
        $instance = wp_parse_args($instance, $defaults);
        extract($instance);
        $args = array(
            'type'                     => 'project',
            'child_of'                 => 0,
            'parent'                   => '',
            'orderby'                  => 'name',
            'order'                    => 'ASC',
            'hide_empty'               => 1,
            'hierarchical'             => 1,
            'exclude'                  => '',
            'include'                  => '',
            'number'                   => '',
            'taxonomy'                 => 'project-category',
            'pad_counts'               => false

        );

        $categories = get_categories( $args );
        if(empty($categories)){$cats[]= esc_html__('None', 'funding');}else{
        foreach ($categories as $cat) {
            $cats[$cat->cat_ID] = $cat->cat_name;
        }}

        ?>
         <p class="description">
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">
                <?php esc_html_e("Title (optional)", 'funding'); ?>
                <?php echo field_input('title', $block_id, $title, $size = 'full') ?>
            </label>
         </p>
         <p class="description">
            <label for="<?php echo esc_attr($this->get_field_id('newscats')); ?>">
                <?php esc_html_e("Check categories you want to include", 'funding'); ?><br />
                <?php echo field_checkboxfromarray('newscats', $block_id, $cats, $instance['newscats']) ?>
            </label>
        </p>
        <p class="description fourth">
            <label for="<?php echo esc_attr( $this->get_field_id('projectdispl')); ?>">
                <?php esc_html_e("Pick display option", 'funding'); ?><br/>
                <?php echo field_select('projectdispl', $block_id, $line_options, $projectdispl, $block_id); ?>
            </label>
        </p>
        <p class="description fourth">
            <label for="<?php echo esc_attr($this->get_field_id('slidenum')); ?>">
                <?php esc_html_e("Insert number of slide posts", 'funding'); ?>
                <?php echo field_input('slidenum', $block_id, $slidenum, $size = 'full') ?>
            </label>
        </p>
        <p class="description fourth">
            <label for="<?php echo esc_attr($this->get_field_id('staticnum')); ?>">
                <?php esc_html_e("Insert number of static posts", 'funding'); ?>
                <?php echo field_input('staticnum', $block_id, $staticnum, $size = 'full') ?>
            </label>
        </p>
        <br /><br /><br /><br />
        <div>
         <p class="description">
            <label for="<?php echo esc_attr($this->get_field_id('bstatic')); ?>">
             <?php esc_html_e("Show view all projects button in static projects", 'funding'); ?>
                <?php echo field_checkbox('bstatic', $block_id, $bstatic, $check = 'true') ?>
            </label>
        </p>
         <p class="description">
            <label for="<?php echo esc_attr($this->get_field_id('bslider')); ?>">
             <?php esc_html_e("Show view all projects button in slider projects", 'funding'); ?>
                <?php echo field_checkbox('bslider', $block_id, $bslider, $check = 'true') ?>
            </label>
        </p>
        </div>
        <?php
    }


    function pbblock($instance) {
        extract($instance); ?>


        <script>
        /**********************projects carousel *******************/
        jQuery(document).ready(function() {
           jQuery("#foo<?php echo esc_attr($block_id); ?>").carouFredSel({
                auto : false,
                height: 'variable',
                prev : "#foo<?php echo esc_attr($block_id);?>_prev",
                next : "#foo<?php echo esc_attr($block_id);?>_next",
                responsive: true,
                swipe: {
                    onTouch: true,
                    onMouse: true
                },
                items: {
                    width: 300,
                    height: 'variable',
                    visible:{
                        min: 1,
                        max: 4
                    }
                },
                scroll : {
                        items:{
                            width: 900,
                            visible:{
                                    min: 1,
                                    max: 4
                            }
                        },
                },
            });
        });
        </script>
            <?php
            switch($projectdispl) {

            case 'static': /////////////////////////////////////static

               if($title != ""){?><div class="title"><h4><?php echo esc_attr($title); ?></h4></div><?php }

               global $post;

               $args = array (
                     'showposts' => $staticnum,
                     'post_type' => 'project',
                     'orderby' => 'post_date',
                    'tax_query' => array(
                            array(
                                'taxonomy' => 'project-category',
                                'field' => 'id',
                                'terms' => $newscats,
                                'operator' => 'IN')
                        ));
               $posts = get_posts( $args ); ?>
                <div class="isoprblck">
                <?php
                foreach ( $posts as $post ) {
                setup_postdata($post);
                 global $f_currency_signs;
                $project_settings = (array) get_post_meta($post->ID, 'settings', true);

				if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

				if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}


                	if (strpos( $project_settings['date'] , "/") !== false) {
  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
				}else{
					$parseddate = $project_settings['date'];
				}
            	$project_expired = strtotime($parseddate) < time();
                $project_currency_sign = $f_currency_signs[$project_settings['currency']];
                $target= $project_settings['target'];

                $rewards = get_children(array(
                'post_parent' => $post->ID,
                'post_type' => 'reward',

                'order' => 'ASC',
                'orderby' => 'meta_value_num',
                'meta_key' => 'funding_amount',
            ));

            $funders = array();
            $funded_amount = 0;
            $chosen_reward = null;

            foreach($rewards as $this_reward){
                $these_funders = get_children(array(
                    'post_parent' => $this_reward->ID,
                    'post_type' => 'funder',
                    'post_status' => 'publish'
                ));
                foreach($these_funders as $this_funder){
                    $funding_amount = get_post_meta($this_funder->ID, 'funding_amount', true);
                    $funders[] = $this_funder;
                    $funded_amount += $funding_amount;
                }
            }

              if(empty($target) or $target == 0){$target = 1;}
     ?>
                <div class="project-card">
             <?php if(has_post_thumbnail()){
                    $thumb = get_post_thumbnail_id();
                    $img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
                    $image = aq_resize( $img_url, 311, 210, true, '', true ); //resize & crop img
                ?>
              <div class="project-thumb-wrapper"><a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url($image[0]); ?>" /></a></div>
                <?php
                }else{ ?>
                <div class="project-thumb-wrapper"><a href="<?php the_permalink(); ?>"><img class="pbimage" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/defaults/default_project.jpg"></a></div>
                <?php } ?>



            <h5 class="bbcard_name"><a href="<?php the_permalink(); ?>"><?php $title = get_the_title(); echo esc_attr(mb_substr($title, 0,23)); if(strlen($title) > 23){echo '...';}?></a></h5>
			<?php if(get_the_author_meta('first_name',get_the_author_meta('ID')) or get_the_author_meta('last_name',get_the_author_meta('ID'))){ ?><span><?php esc_html_e("by", 'funding'); ?> <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php echo esc_attr(get_the_author_meta('first_name',get_the_author_meta('ID')).' '.get_the_author_meta('last_name',get_the_author_meta('ID'))); ?></a></span> <?php } ?>
            <p> <?php
                $excerpt = get_the_excerpt();
                echo mb_substr($excerpt, 0,110);echo '...';
             ?></p>
			  <p>
                <?php if(usercountry_name_display(get_the_author_meta( 'ID' )) != '' || get_the_author_meta('city', get_the_author_meta( 'ID' )) != ''){ ?>
                <span class="icon-map-marker" ></span> <b><?php echo esc_attr(usercountry_name_display(get_the_author_meta( 'ID' )));?></b><?php if(get_the_author_meta('city', get_the_author_meta( 'ID' )) != ''){ echo ', '; } ?>
                <?php if ( get_the_author_meta('city', get_the_author_meta( 'ID' )) ) {echo esc_attr(get_the_author_meta('city',get_the_author_meta( 'ID' ))); } ?>
                <?php } ?>
            </p>

            <?php if($funded_amount == $target or $funded_amount > $target){ ?>
                <div class="project-successful">
                    <strong><?php esc_html_e('Successful!', 'funding') ?></strong>
                </div>
            <?php }elseif($project_expired){ ?>

                        <div class="project-unsuccessful">
                            <strong><?php esc_html_e('Unsuccessful!', 'funding') ?></strong>
                        </div>

            <?php }else{ ?>
            <div class="progress progress-striped active bar-green"><div style="width: <?php printf(esc_html__('%u%', 'funding'), round($funded_amount/$target*100), $project_currency_sign, round($target)) ?>%" class="bar"></div></div>
            <?php } ?>
            <ul class="project-stats">
                <li class="first funded">
                     <strong><?php printf(esc_html__('%u%%', 'funding'), round($funded_amount/$target*100), $project_currency_sign, number_format(round((int)$target), 0, '.', ',')) ?></strong><?php esc_html_e('funded', 'funding'); ?>
                </li>
                <li class="pledged">
                    <strong>
                        <?php print $project_currency_sign; print number_format(round((int)$target), 0, '.', ',')?></strong><?php esc_html_e('target', 'funding'); ?>
                </li>
                <li data-end_time="2013-02-24T08:41:18Z" class="last ksr_page_timer">
                     <?php
                    if(!$project_expired) : ?>
                        <?php if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){ ?> <strong> <?php esc_html_e('< 24', 'funding'); ?></strong> <?php }else{ ?>
                        <strong><?php print F_Controller::timesince(time(), strtotime($parseddate), 1, ''); } ?></strong>
                        <?php if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){ ?>
                         <?php esc_html_e('hours to go', 'funding'); ?>
                        <?php }else{ ?>
                        	<?php if(F_Controller::timesince(time(), strtotime($parseddate), 1, '') == 1){ ?>
                        		 <?php esc_html_e('day to go', 'funding'); ?>
                        	<?php }else{ ?>
                        		 <?php esc_html_e('days to go', 'funding'); ?>
                        	<?php } ?>


                        <?php } ?>
                    <?php endif; ?>
                </li>
            </ul>

           <div class="clear"></div>
        </div>



<?php }wp_reset_query(); ?>
  </div><div class="clear"></div>
  <?php if($bstatic){ ?>

      <a class="edit-button button-small button-green" href="<?php echo get_permalink( get_page_by_path( 'all-projects' ) ); ?>"><?php esc_html_e("View all projects", 'funding'); ?></a>
  <div class="clear"></div>
<?php }

    break;

    case 'slide': /////////////////////////////////////////slide
if($title != ""){?><div class="title"><h4><?php echo esc_attr($title); ?></h4></div><?php } ?>
    <div class="image_carousel">
    <div id="foo<?php echo esc_attr($block_id);?>">
<?php

               global $post;

               $args = array (
                     'showposts' => $slidenum,
                     'post_type' => 'project',
                     'orderby' => 'post_date',
                    'tax_query' => array(
                            array(
                                'taxonomy' => 'project-category',
                                'field' => 'id',
                                'terms' => $newscats,
                                'operator' => 'IN')
                        ));
               $posts = get_posts( $args ); ?>

                <?php
                foreach ( $posts as $post ) {
                setup_postdata($post);

                global $f_currency_signs;
                $project_settings = (array) get_post_meta($post->ID, 'settings', true);

				if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

				if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				if(!isset($array[1]))$array[1]= '';
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}


                	if (strpos( $project_settings['date'] , "/") !== false) {
  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
			}else{
				$parseddate = $project_settings['date'];
			}
            	$project_expired = strtotime($parseddate) < time();
                $project_currency_sign = $f_currency_signs[$project_settings['currency']];
                $target= $project_settings['target'];

                $rewards = get_children(array(
                'post_parent' => $post->ID,
                'post_type' => 'reward',

                'order' => 'ASC',
                'orderby' => 'meta_value_num',
                'meta_key' => 'funding_amount',
            ));

            $funders = array();
            $funded_amount = 0;
            $chosen_reward = null;

            foreach($rewards as $this_reward){
                $these_funders = get_children(array(
                    'post_parent' => $this_reward->ID,
                    'post_type' => 'funder',
                    'post_status' => 'publish'
                ));
                foreach($these_funders as $this_funder){
                    $funding_amount = get_post_meta($this_funder->ID, 'funding_amount', true);
                    $funders[] = $this_funder;
                    $funded_amount += $funding_amount;
                }
            }

            if(empty($target) or $target == 0){$target = 1;}
?>
          <div class="project-card">
            <?php if(has_post_thumbnail()){
                    $thumb = get_post_thumbnail_id();
                    $img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
                    $image = aq_resize( $img_url, 311, 210, true, '', true ); //resize & crop img
                ?>
              <div class="project-thumb-wrapper"><a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url($image[0]); ?>" /></a></div>
                <?php
                }else{ ?>
                <div class="project-thumb-wrapper"><a href="<?php the_permalink(); ?>"><img class="pbimage" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/defaults/default_project.jpg"></a></div>
                <?php } ?>


            <h5 class="bbcard_name"><a href="<?php the_permalink(); ?>"><?php $title = get_the_title(); echo esc_attr(mb_substr($title, 0,23)); if(strlen($title) > 23){echo '...';}?></a></h5>
			<?php if(get_the_author_meta('first_name',get_the_author_meta('ID')) or get_the_author_meta('last_name',get_the_author_meta('ID'))){ ?><span><?php esc_html_e("by", 'funding'); ?> <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php echo esc_attr(get_the_author_meta('first_name',get_the_author_meta('ID')).' '.get_the_author_meta('last_name',get_the_author_meta('ID'))); ?></a></span> <?php } ?>
            <p> <?php
                $excerpt = get_the_excerpt();
                echo mb_substr($excerpt, 0,110);echo '...';
             ?></p>
			 <p>
                <?php if(usercountry_name_display(get_the_author_meta( 'ID' )) != '' || get_the_author_meta('city', get_the_author_meta( 'ID' )) != ''){ ?>
                <span class="icon-map-marker" ></span> <b><?php echo esc_attr(usercountry_name_display(get_the_author_meta( 'ID' )));?></b><?php if(get_the_author_meta('city', get_the_author_meta( 'ID' )) != ''){ echo ', '; } ?>
                <?php if ( get_the_author_meta('city', get_the_author_meta( 'ID' )) ) {echo esc_attr(get_the_author_meta('city',get_the_author_meta( 'ID' ))); } ?>
                <?php } ?>
            </p>

            <?php if($funded_amount == $target or $funded_amount > $target){ ?>
                <div class="project-successful">
                    <strong><?php esc_html_e('Successful!', 'funding') ?></strong>
                </div>
            <?php }elseif($project_expired){ ?>

                        <div class="project-unsuccessful">
                            <strong><?php esc_html_e('Unsuccessful!', 'funding') ?></strong>
                        </div>

            <?php }else{ ?>
            <div class="progress progress-striped active bar-green"><div style="width: <?php printf(esc_html__('%u%', 'funding'), round($funded_amount/$target*100), $project_currency_sign, round($target)) ?>%" class="bar"></div></div>
            <?php } ?>
            <ul class="project-stats">
                <li class="first funded">
                     <strong><?php printf(esc_html__('%u%%', 'funding'), round($funded_amount/$target*100), $project_currency_sign, number_format(round((int)$target), 0, '.', ',')) ?></strong><?php esc_html_e( 'funded', 'funding'); ?>
                </li>
                <li class="pledged">
                    <strong>
                        <?php print $project_currency_sign; print number_format(round((int)$target), 0, '.', ',')?></strong><?php esc_html_e('target', 'funding'); ?>
                </li>
                <li data-end_time="2013-02-24T08:41:18Z" class="last ksr_page_timer">
                     <?php
                    if(!$project_expired) : ?>
                        <?php if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){ ?> <strong> <?php esc_html_e('< 24', 'funding'); ?></strong> <?php }else{ ?>
                        <strong><?php print F_Controller::timesince(time(), strtotime($parseddate), 1, ''); } ?></strong>
                        <?php if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){ ?>
                         <?php esc_html_e('hours to go', 'funding'); ?>
                        <?php }else{ ?>
                        	<?php if(F_Controller::timesince(time(), strtotime($parseddate), 1, '') == 1){ ?>
                        		 <?php esc_html_e('day to go', 'funding'); ?>
                        	<?php }else{ ?>
                        		 <?php esc_html_e('days to go', 'funding'); ?>
                        	<?php } ?>


                        <?php } ?>
                    <?php endif; ?>
                </li>
            </ul>

           <div class="clear"></div>
        </div>

<?php }wp_reset_query(); ?>
  </div> <div class="clearfix"></div>
 <a class="prev" id="foo<?php echo esc_attr($block_id);?>_prev" href="#">&nbsp;</a>
 <a class="next" id="foo<?php echo esc_attr($block_id);?>_next" href="#">&nbsp;</a><div class="clear"></div></div>
  <?php if($bslider){ ?>
      <a class="edit-button button-small button-green" href="<?php echo get_permalink( get_page_by_path( 'tmp-all-projects' ) ); ?>"><?php esc_html_e("View all projects", 'funding'); ?></a>
  <div class="clear"></div>
<?php }


break;



case 'all': //////////////////////////////////all
if($title != ""){?><div class="title"><h4><?php echo esc_attr($title); ?></h4></div><?php } ?>

    <div class="image_carousel">
    <div id="foo<?php echo esc_attr($block_id); ?>">
<?php

               global $post;

               $args = array (
                     'showposts' => $slidenum,
                     'post_type' => 'project',
                     'orderby' => 'post_date',
                    'tax_query' => array(
                            array(
                                'taxonomy' => 'project-category',
                                'field' => 'id',
                                'terms' => $newscats,
                                'operator' => 'IN')
                        ));
               $posts = get_posts( $args ); ?>

                <?php
                foreach ( $posts as $post ) {
                setup_postdata($post);

                global $f_currency_signs;
                $project_settings = (array) get_post_meta($post->ID, 'settings', true);

				if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

				if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}


               	if (strpos( $project_settings['date'] , "/") !== false) {
  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
			}else{
				$parseddate = $project_settings['date'];
			}
            	$project_expired = strtotime($parseddate) < time();
                $project_currency_sign = $f_currency_signs[$project_settings['currency']];
                $target= $project_settings['target'];

                $rewards = get_children(array(
                'post_parent' => $post->ID,
                'post_type' => 'reward',

                'order' => 'ASC',
                'orderby' => 'meta_value_num',
                'meta_key' => 'funding_amount',
            ));

            $funders = array();
            $funded_amount = 0;
            $chosen_reward = null;

            foreach($rewards as $this_reward){
                $these_funders = get_children(array(
                    'post_parent' => $this_reward->ID,
                    'post_type' => 'funder',
                    'post_status' => 'publish'
                ));
                foreach($these_funders as $this_funder){
                    $funding_amount = get_post_meta($this_funder->ID, 'funding_amount', true);
                    $funders[] = $this_funder;
                    $funded_amount += $funding_amount;
                }
            }

            if(empty($target) or $target == 0){$target = 1;}
?>
          <div class="project-card">
            <?php if(has_post_thumbnail()){
                    $thumb = get_post_thumbnail_id();
                    $img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
                    $image = aq_resize( $img_url, 311, 210, true, '', true ); //resize & crop img
                ?>
              <div class="project-thumb-wrapper"><a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url($image[0]); ?>" /></a></div>
                <?php
                }else{ ?>
                <div class="project-thumb-wrapper"><a href="<?php the_permalink(); ?>"><img class="pbimage" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/defaults/default_project.jpg"></a></div>
                <?php } ?>


            <h5 class="bbcard_name"><a href="<?php the_permalink(); ?>"><?php $title = get_the_title(); echo esc_attr(mb_substr($title, 0,23)); if(strlen($title) > 23){echo '...';}?></a></h5>
			<?php if(get_the_author_meta('first_name',get_the_author_meta('ID')) or get_the_author_meta('last_name',get_the_author_meta('ID'))){ ?><span><?php esc_html_e("by", 'funding'); ?> <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php echo esc_attr(get_the_author_meta('first_name',get_the_author_meta('ID')).' '.get_the_author_meta('last_name',get_the_author_meta('ID'))); ?></a></span> <?php } ?>
            <p> <?php
                $excerpt = get_the_excerpt();
                echo mb_substr($excerpt, 0,110);echo '...';
             ?></p>
			 <p>
                <?php if(usercountry_name_display(get_the_author_meta( 'ID' )) != '' || get_the_author_meta('city', get_the_author_meta( 'ID' )) != ''){ ?>
                <span class="icon-map-marker" ></span> <b><?php echo esc_attr(usercountry_name_display(get_the_author_meta( 'ID' )));?></b><?php if(get_the_author_meta('city', get_the_author_meta( 'ID' )) != ''){ echo ', '; } ?>
                <?php if ( get_the_author_meta('city', get_the_author_meta( 'ID' )) ) {echo esc_attr(get_the_author_meta('city',get_the_author_meta( 'ID' ))); } ?>
                <?php } ?>
            </p>

            <?php if($funded_amount == $target or $funded_amount > $target){ ?>
                <div class="project-successful">
                    <strong><?php esc_html_e('Successful!', 'funding') ?></strong>
                </div>
            <?php }elseif($project_expired){ ?>

                        <div class="project-unsuccessful">
                            <strong><?php esc_html_e('Unsuccessful!', 'funding') ?></strong>
                        </div>

            <?php }else{ ?>
            <div class="progress progress-striped active bar-green"><div style="width: <?php printf(esc_html__('%u%', 'funding'), round($funded_amount/$target*100), $project_currency_sign, round($target)) ?>%" class="bar"></div></div>
            <?php } ?>
            <ul class="project-stats">
                <li class="first funded">
                     <strong><?php printf(esc_html__('%u%%', 'funding'), round($funded_amount/$target*100), $project_currency_sign, number_format(round((int)$target), 0, '.', ',')) ?></strong><?php esc_html_e( 'funded', 'funding'); ?>
                </li>
                <li class="pledged">
                    <strong>
                        <?php print $project_currency_sign; print number_format(round((int)$target), 0, '.', ',')?></strong><?php esc_html_e('target', 'funding'); ?>
                </li>
                <li data-end_time="2013-02-24T08:41:18Z" class="last ksr_page_timer">
                     <?php
                    if(!$project_expired) : ?>
                        <?php if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){ ?> <strong> <?php esc_html_e('< 24', 'funding'); ?></strong> <?php }else{ ?>
                        <strong><?php print F_Controller::timesince(time(), strtotime($parseddate), 1, ''); } ?></strong>
                        <?php if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){ ?>
                         <?php esc_html_e('hours to go', 'funding'); ?>
                        <?php }else{ ?>
                        	<?php if(F_Controller::timesince(time(), strtotime($parseddate), 1, '') == 1){ ?>
                        		 <?php esc_html_e('day to go', 'funding'); ?>
                        	<?php }else{ ?>
                        		 <?php esc_html_e('days to go', 'funding'); ?>
                        	<?php } ?>


                        <?php } ?>
                    <?php endif; ?>
                </li>
            </ul>

           <div class="clear"></div>
        </div>

<?php }wp_reset_query(); ?>
  </div> <div class="clearfix"></div>
 <a class="prev" id="foo<?php echo esc_attr($block_id);?>_prev" href="#">&nbsp;</a>
 <a class="next" id="foo<?php echo esc_attr($block_id);?>_next" href="#">&nbsp;</a><div class="clear"></div></div>

<?php
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

               global $post;

               $args = array (
                     'showposts' => $staticnum,
                     'post_type' => 'project',
                     'orderby' => 'post_date',
                    'tax_query' => array(
                            array(
                                'taxonomy' => 'project-category',
                                'field' => 'id',
                                'terms' => $newscats,
                                'operator' => 'IN')
                        ));
               $posts = get_posts( $args ); ?>
                <div class="isoprblck">
                <?php
                foreach ( $posts as $post ) {
                setup_postdata($post);
                 global $f_currency_signs;
                $project_settings = (array) get_post_meta($post->ID, 'settings', true);

				if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

				if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}


                	if (strpos( $project_settings['date'] , "/") !== false) {
	  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
				}else{
					$parseddate = $project_settings['date'];
				}
            	$project_expired = strtotime($parseddate) < time();
                $project_currency_sign = $f_currency_signs[$project_settings['currency']];
                $target= $project_settings['target'];

                $rewards = get_children(array(
                'post_parent' => $post->ID,
                'post_type' => 'reward',

                'order' => 'ASC',
                'orderby' => 'meta_value_num',
                'meta_key' => 'funding_amount',
            ));

            $funders = array();
            $funded_amount = 0;
            $chosen_reward = null;

            foreach($rewards as $this_reward){
                $these_funders = get_children(array(
                    'post_parent' => $this_reward->ID,
                    'post_type' => 'funder',
                    'post_status' => 'publish'
                ));
                foreach($these_funders as $this_funder){
                    $funding_amount = get_post_meta($this_funder->ID, 'funding_amount', true);
                    $funders[] = $this_funder;
                    $funded_amount += $funding_amount;
                }
            }

              if(empty($target) or $target == 0){$target = 1;}
     ?>
                <div class="project-card span3">
             <?php if(has_post_thumbnail()){
                    $thumb = get_post_thumbnail_id();
                    $img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
                    $image = aq_resize( $img_url, 311, 210, true, '', true ); //resize & crop img
                ?>
              <div class="project-thumb-wrapper"><a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url($image[0]); ?>" /></a></div>
                <?php
                }else{ ?>
                <div class="project-thumb-wrapper"><a href="<?php the_permalink(); ?>"><img class="pbimage" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/defaults/default_project.jpg"></a></div>
                <?php } ?>



            <h5 class="bbcard_name"><a href="<?php the_permalink(); ?>"><?php $title = get_the_title(); echo esc_attr(mb_substr($title, 0,23)); if(strlen($title) > 23){echo '...';}?></a></h5>
			<?php if(get_the_author_meta('first_name',get_the_author_meta('ID')) or get_the_author_meta('last_name',get_the_author_meta('ID'))){ ?><span><?php esc_html_e("by", 'funding'); ?> <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php echo esc_attr(get_the_author_meta('first_name',get_the_author_meta('ID')).' '.get_the_author_meta('last_name',get_the_author_meta('ID'))); ?></a></span> <?php } ?>
            <p> <?php
                $excerpt = get_the_excerpt();
                echo mb_substr($excerpt, 0,110);echo '...';
             ?></p>
			 <p>
                <?php if(usercountry_name_display(get_the_author_meta( 'ID' )) != '' || get_the_author_meta('city', get_the_author_meta( 'ID' )) != ''){ ?>
                <span class="icon-map-marker" ></span> <b><?php echo esc_attr(usercountry_name_display(get_the_author_meta( 'ID' )));?></b><?php if(get_the_author_meta('city', get_the_author_meta( 'ID' )) != ''){ echo ', '; } ?>
                <?php if ( get_the_author_meta('city', get_the_author_meta( 'ID' )) ) {echo esc_attr(get_the_author_meta('city',get_the_author_meta( 'ID' ))); } ?>
                <?php } ?>
            </p>

            <?php if($funded_amount == $target or $funded_amount > $target){ ?>
                <div class="project-successful">
                    <strong><?php esc_html_e('Successful!', 'funding') ?></strong>
                </div>
            <?php }elseif($project_expired){ ?>

                        <div class="project-unsuccessful">
                            <strong><?php esc_html_e('Unsuccessful!', 'funding') ?></strong>
                        </div>

            <?php }else{ ?>
            <div class="progress progress-striped active bar-green"><div style="width: <?php printf(esc_html__('%u%', 'funding'), round($funded_amount/$target*100), $project_currency_sign, round($target)) ?>%" class="bar"></div></div>
            <?php } ?>
            <ul class="project-stats">
                <li class="first funded">
                     <strong><?php printf(esc_html__('%u%%', 'funding'), round($funded_amount/$target*100), $project_currency_sign, number_format(round((int)$target), 0, '.', ',')) ?></strong><?php esc_html_e( 'funded', 'funding'); ?>
                </li>
                <li class="pledged">
                    <strong>
                        <?php print $project_currency_sign; print number_format(round((int)$target), 0, '.', ',')?></strong><?php esc_html_e('target', 'funding'); ?>
                </li>
                <li data-end_time="2013-02-24T08:41:18Z" class="last ksr_page_timer">
                     <?php
                    if(!$project_expired) : ?>
                        <?php if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){ ?> <strong> <?php esc_html_e('< 24', 'funding'); ?></strong> <?php }else{ ?>
                        <strong><?php print F_Controller::timesince(time(), strtotime($parseddate), 1, ''); } ?></strong>
                        <?php if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){ ?>
                         <?php esc_html_e('hours to go', 'funding'); ?>
                        <?php }else{ ?>
                        	<?php if(F_Controller::timesince(time(), strtotime($parseddate), 1, '') == 1){ ?>
                        		 <?php esc_html_e('day to go', 'funding'); ?>
                        	<?php }else{ ?>
                        		 <?php esc_html_e('days to go', 'funding'); ?>
                        	<?php } ?>


                        <?php } ?>
                    <?php endif; ?>
                </li>
            </ul>

           <div class="clear"></div>
        </div>



<?php }wp_reset_query(); ?>
  </div><div class="clear"></div>
  <?php if($bstatic){ ?>

      <a class="edit-button button-small button-green" href="<?php echo get_permalink( get_page_by_path( 'all-projects' ) ); ?>"><?php esc_html_e("View all projects", 'funding'); ?></a>
  <div class="clear"></div>
<?php }

break;
}
  }
}