<?php
/** A simple text block **/
class Carousel_Block extends Block {
   function __construct() {
            $block_options = array(
                'name' => esc_html__('Carousel', 'funding'),
                'size' => 'span12',
            );
            //create the widget
            parent::__construct('Carousel_Block', $block_options);
            //add ajax functions
            add_action('wp_ajax_block_item_add_new', array($this, 'add_item'));
        }
        function form($instance) {
            $defaults = array(
                'textitem' => '',
                'carousel_style' => 'autoplay',
                'items' => array(
                    1 => array(
                        'title' => esc_html__('New carousel item', 'funding'),
                        'image' => '',
                    )
                ),
                'type'  => 'item',
            );
            $carousel_options = array(
            'none' => esc_html__('Default carousel','funding'),
            'autoplay' => esc_html__('Autoplay carousel','funding'),
            'ticker' => esc_html__('Ticker carousel','funding'),
            //'image' => 'Use Image',
        );
            $instance = wp_parse_args($instance, $defaults);
            extract($instance);
            $item_types = array(
                'item' => 'Items',
        );
            ?>
            <div class="description cf">
             <p class="description">
            <label for="<?php echo $this->get_field_id('title') ?>">
                <?php esc_html_e("Title (optional)", 'funding'); ?>
                <?php echo field_input('title', $block_id, $title, $size = 'full') ?>
            </label>
            </p>
             <p class="description">
            <label for="<?php echo $this->get_field_id('carousel_style') ?>">
                <?php esc_html_e("Select carousel style:", 'funding'); ?>
                <?php echo field_select('carousel_style', $block_id, $carousel_options, $carousel_style, $block_id); ?>
            </label>
            </p>
                <ul id="sortable-list-<?php echo $block_id ?>" class="sortable-list" rel="<?php echo $block_id ?>">
                    <?php
                    $items = is_array($items) ? $items : $defaults['items'];
                    $count = 1;
                    foreach($items as $item) {
                        $this->item($item, $count);
                        $count++;
                    }
                    ?>
                </ul>
                <p></p>
                <a href="#" rel="item" class="sortable-add-new button"><?php esc_html_e("Add New", 'funding'); ?></a>
                <p></p>
            </div>
            <?php
        }
        function item($item = array(), $count = 0) {
    ?>
            <li id="<?php echo $this->get_field_id('items') ?>-sortable-item-<?php echo $count ?>" class="sortable-item" rel="<?php echo $count ?>">
                <div class="sortable-head cf">
                    <div class="sortable-title">
                        <strong><?php echo $item['title'] ?></strong>
                    </div>
                    <div class="sortable-handle">
                        <a href="#"><?php esc_html_e("Open / Close", 'funding'); ?></a>
                    </div>
                </div>
                <div class="sortable-body">
                    <p class="item-desc description">
                        <label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-title">
                            <?php esc_html_e("Item", 'funding') ?><br/>
                            <input type="text" id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-title" class="input-full" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][title]" value="<?php echo $item['title'] ?>" />
                        </label>
                    </p>
<!--                    <p class="item-desc description">
                        <label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-image">
                            <?php esc_html_e("Item image link", 'funding') ?><br/>
                            <input type="text" id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-image" class="input-full" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][image]" value="<?php echo $item['image'] ?>" />
                        </label>
                    </p>-->
                    <p class="item-desc description">
                        <label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-image">
                            <?php esc_html_e("Item image link", 'funding') ?><br/>
                            <?php echo field_upload_for_carusel("[items][".$count."][image]",$this->get_field_id('items'),$item['image'],'image');?>
                        </label>
                    </p>

                     <p class="item-desc description">
                        <label for="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-link">
                            <?php esc_html_e("Item external link", 'funding') ?><br/>
                            <input type="text" id="<?php echo $this->get_field_id('items') ?>-<?php echo $count ?>-link" class="input-full" name="<?php echo $this->get_field_name('items') ?>[<?php echo $count ?>][link]" value="<?php echo $item['link'] ?>" />
                        </label>
                    </p>
                    <p class="item-desc description"><a href="#" class="sortable-delete"> <?php esc_html_e("Delete", 'funding') ?></a></p>
                </div>
            </li>
            <?php
        }
        function pbblock($instance) {
            extract($instance);
            if($title) echo '<h3>'.$title.'</h3>';
            echo '<div class="bx-wrapper" ><div class="bx-viewport" ><ul class="h-logo-carousel h-logo-carousel-'.$block_id.'">';
            wp_enqueue_script('jquery-ui-tabs');
            $output = '';
                    $i = 1;
                    foreach( $items as $item ){
                        if(empty($item['link'])){
                      $output .= '<li><img alt="Alt text" src="'.$item['image'].'" /></li>';
                      }else{
                       if(substr($item['link'], 0, 7) != 'http://'){if(substr($item['link'], 0, 8) == 'https://'){}else{$item['link'] = 'http://'.$item['link'];}}
                       $output .= '<li><a href="'.$item['link'].'" target="_blank"><img alt="Alt text" src="'.$item['image'].'" /></a></li>';
                      }
                      $i++;
                    }
            echo $output;
            echo '</ul><div class="clear"></div></div>

            </div>';?>

            <?php switch($carousel_style) {
                    case 'none': ?>
                    <script>
                    jQuery( document ).ready(function() {
                    jQuery(".h-logo-carousel-<?php echo $block_id;?>").bxSlider({
                        mode: "horizontal",
                        speed: 500,
                        slideMargin: 0,
                        infiniteLoop: true,
                        hideControlOnEnd: false,
                        captions: false,
                        ticker: false,
                        tickerHover: false,
                        adaptiveHeight: false,
                        responsive: true,
                        pager: false,
                        controls: true,
                        autoControls: false,
                        minSlides: 4,
                        maxSlides: 4,
                        moveSlides: 1,
                        slideWidth: 200,
                        auto: false,
                        pause: 4000,
                        useCSS: false
                    }); });</script>
                    <?php break;
                    case 'autoplay': ?>
                      <script>
                    jQuery( document ).ready(function() {
                  jQuery(".h-logo-carousel-<?php echo $block_id;?>").bxSlider({
                      mode: "horizontal",
                      speed: 500,
                      slideMargin: 0,
                      infiniteLoop: true,
                      hideControlOnEnd: false,
                      captions: false,
                      ticker: false,
                      tickerHover: false,
                      adaptiveHeight: true,
                      responsive: true,
                      pager: false,
                      controls: true,
                      autoControls: false,
                      minSlides: 1,
                      maxSlides: 4,
                      moveSlides: 1,
                      slideWidth: 200,
                      auto: true,
                      pause: 4000,
                      new: "empty",
                      useCSS: false
                  });});</script>
                   <?php break;

                    case 'ticker': ?>
                      <script>
                    jQuery( document ).ready(function() {
                  jQuery(".h-logo-carousel-<?php echo $block_id;?>").bxSlider({
                            mode: "horizontal",
                            speed: 6000,
                            slideMargin: 0,
                            infiniteLoop: true,
                            hideControlOnEnd: false,
                            captions: false,
                            ticker: true,
                            tickerHover: true,
                            adaptiveHeight: false,
                            responsive: true,
                            pager: false,
                            controls: true,
                            autoControls: false,
                            minSlides: 1,
                            maxSlides: 4,
                            moveSlides: 1,
                            slideWidth: 200,
                            auto: false,
                            pause: 4000,
                            useCSS: false
                        });});</script>
                    <?php break;

        }

    }
        /* AJAX add item */
        function add_item() {
            $nonce = $_POST['security'];
            if (! wp_verify_nonce($nonce, 'pb-settings-page-nonce') ) die('-1');
            $count = isset($_POST['count']) ? absint($_POST['count']) : false;
            $this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'block-9999';
            //default key/value for the item
            $item = array(
                'title' => esc_html__('New Item', 'funding'),
                'image' => ''
            );
            if($count) {
                $this->item($item, $count);
            } else {
                die(-1);
            }
            die();
        }
        function update($new_instance, $old_instance) {
            $new_instance = recursive_sanitize($new_instance);
            return $new_instance;
        }
}