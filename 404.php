<?php get_header(); ?>

<div class="container page">
    <div class="row">
        <div class="span12">
           <div class="four0four">
    <p class="huge"><?php esc_html_e(' OOPS! 404 ', 'funding');?></p>
    <?php esc_html_e('Page not found, sorry', 'funding');?> :(

            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>