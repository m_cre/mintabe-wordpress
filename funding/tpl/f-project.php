<?php

/**
 * This is the default template for rendering a single project page
 */

?>
<?php get_header(); the_post(); global $post, $new_childpost_id; ?>

<?php

	$new_childpost_id = get_post_meta($post->ID, '_comment_holder_id', true);

	if (strlen($new_childpost_id) == 0) {

		$new_post = array(
		  'post_content'   => 'Comment holder for post: '.$post->post_title,
		  'post_title'     => 'Comment holder for post: '.$post->post_title,
		  'post_status'    => 'publish',
		  'post_type'      => 'comments_holder',
		  'post_parent'    => $post->ID,
		  'comment_status' => 'open'
		);
		 $new_post_id =  wp_insert_post( $new_post, false );
		 update_post_meta($post->ID, '_comment_holder_id', $new_post_id);
		 $new_childpost_id = $new_post_id;
	} ?>
<div class="container page">

      <?php if(!empty($_GET['thanks'])) : ?>
            <div class="cf-thanks">
               <div class="alert alert-success">
                    <?php esc_html_e('Thanks for committing to fund our project. We appreciate your support.', 'funding') ?>
                    <?php printf(esc_html__("We'll contact you when we reach our target of %s%s.", 'funding'), $project_currency_sign, round($project_settings['target'])) ?>
                </div>
            </div>
        <?php endif; ?>
    <div class="tabbable"> <!-- Only required for left/right tabs -->
          <?php  global $f_currency_signs;
            $project_settings = (array) get_post_meta($post -> ID, 'settings', true);


			if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

			if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

            	if (strpos( $project_settings['date'] , "/") !== false) {
  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
			}else{
				$parseddate = $project_settings['date'];
			}
            $project_expired = strtotime($parseddate) < time();
            $project_currency_sign = $f_currency_signs[$project_settings['currency']];
            $target = $project_settings['target']; ?>
                <?php if($funded_amount == $target or $funded_amount > $target){ ?>
                  <div class="alert alert-success">
                    <strong><?php esc_html_e('Yay! This project has been successfully funded!', 'funding') ?></strong>
                 </div>
                <?php }elseif(isset($project_expired) && $project_expired == 1){ ?>

                  <div class="alert alert-error">
                      <strong><?php esc_html_e("Unfortunately this project hasn't been funded on time!", 'funding') ?></strong>
                  </div>

                <?php }?>
      <ul class="nav nav-tabs">
        <li class="active"><a class="button-small button-green" data-toggle="tab" href="#tab1"><?php esc_html_e('Home', 'funding');?></a></li>
        <li><a class="button-small button-green" data-toggle="tab" href="#tab3"><?php esc_html_e('Backers', 'funding');?></a></li>


      </ul>

      <div class="tab-content">
        <div id="tab1" class="tab-pane active">

            <div class="span8">
               <?php if(get_post_meta($post->ID, '_smartmeta_video-link-field', true) == ""){ ?>
            <?php if(!has_post_thumbnail()){ ?>
               <div class="project-thumb-wrapper-big"><img src="<?php echo esc_url(get_template_directory_uri()).'/img/defaults/default_project.jpg'?>" /></div>
            <?php }else{ ?>
                <div class="project-thumb-wrapper-big"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full');  ?></a></div>
            <?php } ?>
               <?php }else{ echo get_post_meta($post->ID, '_smartmeta_video-link-field', true);} ?>

               <?php if(of_get_option('share_this')){ ?>
              <div class="project-social">
              	<span class='st_sharethis_hcount' displayText='ShareThis'></span>
              	<span class='st_facebook_hcount' displayText='Facebook'></span>
              	<span class='st_twitter_hcount' displayText='Tweet'></span>
              	<span class='st_email_hcount' displayText='Email'></span>

              </div>
              <?php } ?>

               <ul class="navsin nav-tabsin">
     <?php
    function comnum(){
	  	global $new_childpost_id;
		$blah = wp_count_comments($new_childpost_id);
		echo esc_attr($blah->approved);
	}   ?>
                        <li class="active"><a href="#story" data-toggle="tab"><?php esc_html_e('The story', 'funding'); ?></a></li>
                        <li><a href="#updates" data-toggle="tab"><strong class='ccounter'><?php $comments_count = wp_count_comments( $post->ID ); echo esc_attr($comments_count->approved); ?></strong><?php esc_html_e('Updates', 'funding'); ?></a></li>
                     	<li><a href="#comments" data-toggle="tab"><strong class='ccounter'><?php comnum(); ?></strong><?php esc_html_e('Comments', 'funding'); ?></a></li>
                    </ul>
              <div class="tab-content w_container">
              <div id="story" class="project-content tab-pane active">
                  <?php the_content(); ?>
              </div>
                 <div id="updates" class="tab-pane">

                               <?php comments_template('/short-comments-update.php'); ?>

                    </div>
                  <div id="comments" class="tab-pane">
                         <?php comments_template('/short-comments-update_child.php'); ?>
                    </div>
                    </div>
              <!-- project-content -->
            </div>
            <!-- /.span8 -->
             <?php
                global $post;
                global $f_currency_signs;
                $project_settings = (array) get_post_meta($post->ID, 'settings', true);

				if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

				if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}


                	if (strpos( $project_settings['date'] , "/") !== false) {
	  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
				}else{
					$parseddate = $project_settings['date'];
				}
            	$project_expired = strtotime($parseddate) < time();
                $project_currency_sign = $f_currency_signs[$project_settings['currency']];
                $target= $project_settings['target'];
                if(!empty($rewards)){
                $keys = array_keys($rewards);
                $lowest_reward = $keys[0];
                $funding_minimum = get_post_meta($lowest_reward, 'funding_amount', true);}else{
                $lowest_reward = 0;
                $funding_minimum = get_post_meta($lowest_reward, 'funding_amount', true);}

                $rewards = get_children(array(
                'post_parent' => $post->ID,
                'post_type' => 'reward',

                'order' => 'ASC',
                'orderby' => 'meta_value_num',
                'meta_key' => 'funding_amount',
            ));

            $funders = array();
            $funded_amount = 0;
            $chosen_reward = null;

            foreach($rewards as $this_reward){
                $these_funders = get_children(array(
                    'post_parent' => $this_reward->ID,
                    'post_type' => 'funder',
                    'post_status' => 'publish'
                ));
                foreach($these_funders as $this_funder){
                    $funding_amount = get_post_meta($this_funder->ID, 'funding_amount', true);
                    $funders[] = $this_funder;
                    $funded_amount += $funding_amount;
                }

            }?>


        </div> <!-- /.tab1 -->


          <div id="tab3" class="tab-pane">

            <div class="span8">
                <div id="project-funders">
                	<?php if(!empty($funders)){ ?>
                    <?php foreach($funders as $funder) : ?>
                        <?php
                            $funder_info = get_post_meta($funder->ID, 'funder', true);
                            $amount = get_post_meta($funder->ID, 'funding_amount', true);
                            $reward = get_post($funder->post_parent);
                            $charged = get_post_meta($funder->ID, 'charged', true);
                        ?>

                        <div class="project-backer span12">
                            <div class="span3">
                            	<?php if(get_the_author_meta( 'ID' ) == get_current_user_id()){ ?>
                                <a href="mailto:<?php print $funder_info['email'] ?>" title="<?php printf(esc_html__('Email %s', 'funding'), $funder_info['name']) ?>">
                                    <?php print get_avatar($funder_info['email'], 85) ?>
                                </a>
                                 <div class="name"><?php print $funder_info['name'] ?></a>

                                 </div>
									<?php }else{ ?>
									<?php print get_avatar($funder_info['email'], 85) ?>
                                    <div class="name"><?php print $funder_info['name'] ?>

                                 </div>

									<?php } ?>
                                <div class="loader"></div>
                            </div>

                            <div class="span4">
                                <span class="amount"><?php print $project_currency_sign.$amount ?></span>
                                <?php if(of_get_option("rewards") == 1){ ?> -
                                <span class="reward"><?php print $reward->post_title ?></span>
                                <?php } ?>
                            </div>

                            <?php if(!empty($charged)) : ?>
                                <div class="icon charged"></div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; }else{?> <div class="no-backers"><?php esc_html_e('No backers yet!', 'funding'); ?> </div> <?php } ?>
                    <div class="clear"></div>
                </div>
             </div>
            <!-- /.span8 -->





        </div><!-- backer tab end -->



        <div class="span4">
                <?php include_once('side.php'); ?>
         </div>
            <!-- /.span4 -->

         </div><!-- tab content end -->


    </div> <!-- tabbable -->

</div><!-- container -->
<?php get_footer() ?>