<?php get_header(); ?>

<div class="container page normal-page">
    <div class="row">
        <div class="span12">
<?php if(get_post_type( $post->ID ) == 'post') { ?>
        	<div class="container blog">
  				<div class="row">
				<div class="span8 ">

<?php } ?>
 <?php if(get_post_type( $post->ID ) == 'project') { ?>
<div class="isoprblck">
 <?php } ?>
			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
				    <?php if(get_post_type( $post->ID ) == 'project') { ?>

    <div class="project-card span3">
              <?php if(has_post_thumbnail()){
                    $thumb = get_post_thumbnail_id();
                    $img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
                    $image = aq_resize( $img_url, 311, 210, true, '', true ); //resize & crop img
                ?>
              <div class="project-thumb-wrapper"><a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url($image[0]); ?>" /></a></div>
                <?php
                }else{ ?>
                <div class="project-thumb-wrapper"><a href="<?php the_permalink(); ?>"><img class="pbimage" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/defaults/default_project.jpg"></a></div>
                <?php } ?>
                 <h5 class="bbcard_name"><a href="<?php the_permalink(); ?>"><?php $title = get_the_title(); echo esc_attr(mb_substr($title, 0,20)); if(strlen($title) > 23){echo '...';}?></a></h5>
                 <?php if(get_the_author_meta('first_name',get_the_author_meta('ID')) or get_the_author_meta('last_name',get_the_author_meta('ID'))){ ?><span><?php esc_html_e("by", 'funding'); ?> <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php echo esc_attr(get_the_author_meta('first_name',get_the_author_meta('ID')).' '.get_the_author_meta('last_name',get_the_author_meta('ID'))); ?></a></span> <?php } ?>
              <p> <?php
                $excerpt = get_the_excerpt();
                echo mb_substr($excerpt, 0,110);echo '...';
             ?></p>
                <?php
                global $post;
                global $f_currency_signs;
                $project_settings = (array) get_post_meta($post->ID, 'settings', true);

			   if(get_option('date_format') == 'm/d/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}

			   if(get_option('date_format') == 'd/m/Y' && strtotime($project_settings['date']) != false){
				$array = explode('/', $project_settings['date']);
				$tmp = $array[0];
				$array[0] = $array[1];
				$array[1] = $tmp;
				unset($tmp);
				if($array[0] == NULL){
					$project_settings['date'] = $array[1];
				}else{
				$project_settings['date'] = implode('/', $array);
				}
			}


			    	if (strpos( $project_settings['date'] , "/") !== false) {
			  				$parseddate = str_replace('/' , '.' , $project_settings['date']);
						}else{
							$parseddate = $project_settings['date'];
						}
            	$project_expired = strtotime($parseddate) < time();

                $project_currency_sign = $f_currency_signs[$project_settings['currency']];
                $target= $project_settings['target'];
                $rewards = get_children(array(
                'post_parent' => $post->ID,
                'post_type' => 'reward',
                'order' => 'ASC',
                'orderby' => 'meta_value_num',
                'meta_key' => 'funding_amount',
            ));
            $funders = array();
            $funded_amount = 0;
            $chosen_reward = null;
            foreach($rewards as $this_reward){
                $these_funders = get_children(array(
                    'post_parent' => $this_reward->ID,
                    'post_type' => 'funder',
                    'post_status' => 'publish'
                ));
                foreach($these_funders as $this_funder){
                    $funding_amount = get_post_meta($this_funder->ID, 'funding_amount', true);
                    $funders[] = $this_funder;
                    $funded_amount += $funding_amount;
                }
            } ?>
             <p>
                <?php if(usercountry_name_display(get_the_author_meta( 'ID' )) != '' || get_the_author_meta('city', get_the_author_meta( 'ID' )) != ''){ ?>
                <span class="icon-map-marker" ></span> <b><?php echo esc_attr(usercountry_name_display(get_the_author_meta( 'ID' )));?></b>
                <?php if(get_the_author_meta('city', get_the_author_meta( 'ID' )) != ''){ echo ', '; } ?>
                <?php if ( get_the_author_meta('city', get_the_author_meta( 'ID' )) ) {echo esc_attr(get_the_author_meta('city',get_the_author_meta( 'ID' ))); } ?>
                <?php } ?>
            </p>
              <?php if($funded_amount == $target or $funded_amount > $target){ ?>
                <div class="project-successful">
                    <strong><?php esc_html_e('Successful!', 'funding'); ?></strong>
                </div>
            <?php }elseif($project_expired){ ?>
                        <div class="project-unsuccessful">
                            <strong><?php esc_html_e('Unsuccessful!', 'funding'); ?></strong>
                        </div>
            <?php }else{ ?>
            <div class="progress progress-striped active bar-green"><div style="width: <?php printf(esc_html__('%u%', 'funding'), round($funded_amount/$target*100), $project_currency_sign, number_format(round((int)$target), 0, '.', ',')) ?>%" class="bar"></div></div>
            <?php } ?>
            <ul class="project-stats">
                <li class="first funded">
                     <strong><?php printf(esc_html__('%u%%', 'funding'), round($funded_amount/$target*100), $project_currency_sign, round($target)) ?></strong><?php esc_html_e('funded', 'funding'); ?>
                </li>
                <li class="pledged">
                    <strong>
                         <?php print $project_currency_sign; print number_format(round((int)$target), 0, '.', ',');?></strong><?php esc_html_e('target', 'funding'); ?>
                </li>
                <li data-end_time="2013-02-24T08:41:18Z" class="last ksr_page_timer">
                   <?php
                    if(!$project_expired) : ?>
                        <?php if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){ ?> <strong> <?php esc_html_e('< 24', 'funding'); ?></strong> <?php }else{ ?>
                        <strong><?php print F_Controller::timesince(time(), strtotime($parseddate), 1, ''); } ?></strong>
                        <?php if(strpos(F_Controller::timesince(time(), strtotime($parseddate), 1, ''), "hour")){ ?>
                         <?php esc_html_e('hours to go', 'funding'); ?>
                        <?php }else{ ?>
                        	<?php if(F_Controller::timesince(time(), strtotime($parseddate), 1, '') == 1){ ?>
                        		 <?php esc_html_e('day to go', 'funding'); ?>
                        	<?php }else{ ?>
                        		 <?php esc_html_e('days to go', 'funding'); ?>
                        	<?php } ?>


                        <?php } ?>
                    <?php endif; ?>
                </li>
            </ul>
                <div class="clear"></div>
       </div>
				<?php }else {?>


    <div class="blog-list">

            <?php if ( has_post_thumbnail() ) { ?>
              <div class="blog-pdate green-bg"><?php the_time('M'); ?><br /><?php the_time('d'); ?></div>
              <div class="blog-thumb-wrapper"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('category-thumb');  ?></a></div>

              <?php }else{?>

              <div class="blog-pdate-noimg green-bg"><?php the_time('M'); ?><br /><?php the_time('d'); ?></div>

              <?php } ?>


            <h2><a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></h2>

            <p> <?php the_excerpt(); ?></p>

            <div class="clear"></div>
            <div class="blog-pinfo-wrapper">
                <div class="post-pinfo"><?php esc_html_e("By",'funding');?> <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta( 'ID' ))); ?>" data-toggle="tooltip" data-placement="top" title="<?php esc_html_e("View all posts by ",'funding');?><?php echo esc_attr(get_the_author()); ?>"><?php echo esc_attr(get_the_author()); ?></a> | <a data-toggle="tooltip" data-placement="top" title="<?php comments_number( 'No comments', 'One comment', '% comments' ); ?> in this post" href="<?php echo esc_url(the_permalink()); ?>#comments"> <?php comments_number( 'No comments', 'One comment', '% comments' ); ?></a></div>
                <a class="button-green button-small" href="<?php the_permalink(); ?>"><?php esc_html_e("Read more",'funding');?></a>
                <div class="clear"></div>
            </div>
        </div>
        <!-- /.blog-post -->

			<?php } ?>

			<?php endwhile; ?>
			 <?php if(get_post_type( $post->ID ) == 'project') { ?>
				</div>
			<?php } ?>
			    <?php if(get_post_type( $post->ID ) == 'post') { ?>

			    		</div>
			 <div class="span4 ">
            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer widgets') ) : ?>
                <?php dynamic_sidebar('blog'); ?>
           <?php endif; ?>
    		</div>

    <!-- /.span4 -->
    </div>
 <!-- /.row -->

        <div class="clear"></div>
		<?php } ?>

		 <ul id="pager">
              <li>
                <?php
            $showposts1 = get_option('posts_per_page ');
            $additional_loop = new WP_Query('showposts='.$showposts1.'&paged='.$paged.'&post_type='.get_post_type( $post->ID ));
            $page=$additional_loop->max_num_pages;
            echo kriesi_pagination($additional_loop->max_num_pages);
            ?>
            <?php wp_reset_query(); ?>
              </li>
            </ul>


			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'funding' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php esc_html_e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'funding' ); ?></p>

					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>